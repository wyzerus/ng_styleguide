# Esure Styles/Component Library

[![Build Status](https://travis-ci.com/esure-dev/digital-app-components.svg?token=Qyd4WTD7YYKA8vQCBLrW&branch=master)](https://travis-ci.com/esure-dev/digital-app-components)

This project contains the following libraries:

* [Forms](projects/forms-lib/README.md)
* [Styles](projects/styles-lib/README.md)
* [Utils](projects/utils-lib/README.md)

Plus the showcase application that consumes the libraries with examples of it's usage.

## Quick Start

This will get you the showcase app up and running on `http://localhost:4200/`

```bash
npm install
npm run build:libs
npm start
```

## Development server

Can't see changes? Remember, build libraries before npm start. `npm run build:libs`.

Run `npm start` for a dev server. Opens in `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

### Libraries

Run `npm run build:libs` to build the 3 libraries. The build artifacts will be stored in the `dist/lib-name` directory.
The 3 libraries can also all be built individually.

* npm run build:forms
* npm run build:styles
* npm run build:utils

### App

Run `npm run build` to build the showcase app. The build artifacts will be stored in the `dist/showcase` directory.

## Publish

Run `npm run publish:libs` to publish the 3 built libraries to NPM. This assumes you have updated the version of the package and already built it.
The 3 libraries can also all be published individually.

* npm run publish:forms
* npm run publish:styles
* npm run publish:utils

## Release

As a shortcut, there is also a release command that will do the following for you:

* Increment the library version as a minor update, e.g 0.0.5 becomes 0.0.6
* Build the library
* Publish the library to NPM

Run `npm run release:libs` to release all 3 libraires. The 3 libraires can also all be released individually.

* npm run release:forms
* npm run release:styles
* npm run release:utils

### Updating Changelog

After doing a release, you can run the following to increment the version in the main package.json file and update the CHANGELOG file.

```bash
npm run release
# To do a dry run, i.e just see what changes a release would do
npm run release -- --dry-run
```

## Running unit tests

Run `npm run test:libs` to execute the unit tests for the 3 libraries via [Karma](https://karma-runner.github.io).

## Release Notes

Full release notes can be found [here](CHANGELOG.md).
