import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppPreloader } from '@esure-dev/utils';

/* istanbul ignore next */
const APP_ROUTES: Routes = [
  { path: '', redirectTo: 'showcase', pathMatch: 'full' },
  { path: 'showcase', loadChildren: './routes/showcase/showcase.module#ShowcaseModule', data: { preload: true } },
  { path: 'error', loadChildren: './routes/error/error.module#ErrorModule', data: { preload: false } },
  { path: '**', redirectTo: '/error/404' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(APP_ROUTES, {
      preloadingStrategy: AppPreloader, scrollPositionRestoration: 'enabled',
      anchorScrolling: 'enabled',
    }),
  ],
  providers: [AppPreloader],
  exports: [
    RouterModule,
  ],
})
/* istanbul ignore next */
export class AppRoutingModule {
}
