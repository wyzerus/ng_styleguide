import { Component, HostListener, OnInit } from '@angular/core';
import { Router, Event, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
import { LoadingBoxService } from '@esure-dev/styles';

import { SharedService } from './service/shared.service';
import { primaryRouterFadeAnimation } from './animations/animations';

@Component({
  selector: 'es-app',
  templateUrl: './app.component.html',
  animations: [primaryRouterFadeAnimation],
})
export class AppComponent implements OnInit {

  public innerWidth: number;

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.innerWidth = window.innerWidth;
  }

  constructor(public sharedService: SharedService, private router: Router, private loadingBoxService: LoadingBoxService) {

    this.router.events.subscribe((event: Event) => {
      // Show the loader on navigation start
      if (event instanceof NavigationStart) {
        this.loadingBoxService.showLoader();
      }

      // Hide the loader on any navigation ending events
      if (event instanceof NavigationEnd || event instanceof NavigationCancel || event instanceof NavigationError) {
        this.loadingBoxService.hideLoader();
      }
    });
  }

  ngOnInit() {
    this.innerWidth = window.innerWidth;
    const closeSidebar = this.innerWidth <= 768;
    this.sharedService.burgerActive = !closeSidebar;
    this.sharedService.menuOpen = !closeSidebar;
  }

}
