import { Component, OnInit, Input } from '@angular/core';
import { SharedService } from '../../service/shared.service';

@Component({
  selector: 'es-view-source',
  templateUrl: './view-source.component.html',
})
export class ViewSourceComponent implements OnInit {

  public showSource: boolean = true;

  @Input() ignoreGlobalShowSource: boolean = false;

  constructor(public sharedService: SharedService) {
  }

  ngOnInit() {
  }

  public isHiddenCode(): boolean {
    return !this.showSource || (this.sharedService.hideAllSource && !this.ignoreGlobalShowSource);
  }
}
