import { Component, EventEmitter, HostListener, OnInit, Output } from '@angular/core';
import { SharedService } from '../../service/shared.service';

@Component({
  selector: 'es-sidebar',
  templateUrl: './sidebar.component.html',
})
export class SidebarComponent implements OnInit {

  public innerWidth: number;

  @Output() menuOpen = new EventEmitter<boolean>();

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.innerWidth = window.innerWidth;
  }

  constructor(public sharedService: SharedService) { }
  ngOnInit() {
    this.innerWidth = window.innerWidth;
  }

  toggle() {
    this.emitStateAndToggle();
  }

  closeMenuOnMobile() {
    if (this.innerWidth <= 768) {
      this.emitStateAndToggle();
    }
  }

  emitStateAndToggle() {
    this.menuOpen.emit(this.sharedService.burgerActive);
    this.sharedService.burgerActive = !this.sharedService.burgerActive;
  }

}
