import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {
  ErrorHandlerModule, ErrorHandlerModuleOptions, HttpModule,
  LoggerModule, LoggerModuleOptions, WindowModule, HttpModuleConfig, HTTP_MODULE_CONFIG, WINDOW,
} from '@esure-dev/utils';
import { BackToTopModule, LoadingBoxModule } from '@esure-dev/styles';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routes';
import { SharedService } from './service/shared.service';
import { environment } from '../environments/environment';

const errorHandlerOptions: ErrorHandlerModuleOptions = {
  errorUrl: '/error/500',
};

export function httpModuleConfigFactory(/*win: Window*/): HttpModuleConfig {
  return {
    xapiUrl: '/',
  };
}

const loggerOptions: LoggerModuleOptions = {
  isProduction: environment.production,
};

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    LoggerModule.forRoot(loggerOptions),
    BrowserModule,
    BrowserAnimationsModule,
    BackToTopModule,
    LoadingBoxModule,
    WindowModule,
    HttpClientModule,
    AppRoutingModule,
    ErrorHandlerModule.forRoot(errorHandlerOptions),
    HttpModule.forRoot({
      httpOptionsProvider: {
        provide: HTTP_MODULE_CONFIG,
        useFactory: httpModuleConfigFactory,
        deps: [WINDOW],
      },
    }),
  ],
  providers: [
    SharedService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
