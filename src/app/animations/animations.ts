import { animate, query, style, transition, trigger } from '@angular/animations';

/**
 * Primary router outlet fade transition, use around the most top level router-outlet.
 */
export const primaryRouterFadeAnimation = trigger('primaryRouterFadeAnimation', [
  // The '* => *' will trigger the animation to change between any two states
  transition('* => *', [
    // The query function has three params.
    // First is the event, so this will apply on entering or when the element is added to the DOM.
    // Second is a list of styles or animations to apply.
    // Third we add a config object with optional set to true, this is to signal
    // angular that the animation may not apply as it may or may not be in the DOM.
    query(
      ':enter',
      [style({ position: 'absolute', width: '100%', opacity: 0 })],
      { optional: true },
    ),
    query(
      ':leave',
      // here we apply a style and use the animate function to apply the style over 0.3 seconds
      [style({ position: 'absolute', width: '100%', opacity: 1 }),
      animate('0.3s', style({ opacity: 0 }))],
      { optional: true },
    ),
    query(
      ':enter',
      [style({ opacity: 0 }),
      animate('0.3s', style({ opacity: 1 }))],
      { optional: true },
    ),
  ]),
]);

/**
 * Secondary router outlet fade transition, is currently same as primary but here in case we need to something different
 * for nested router-outlets.
 */
export const secondaryRouterFadeAnimation = trigger('secondaryRouterFadeAnimation', [
  // The '* => *' will trigger the animation to change between any two states
  transition('* => *', [
    // The query function has three params.
    // First is the event, so this will apply on entering or when the element is added to the DOM.
    // Second is a list of styles or animations to apply.
    // Third we add a config object with optional set to true, this is to signal
    // angular that the animation may not apply as it may or may not be in the DOM.
    query(
      ':enter',
      [style({ position: 'absolute', width: '100%', opacity: 0 })],
      { optional: true },
    ),
    query(
      ':leave',
      // here we apply a style and use the animate function to apply the style over 0.3 seconds
      [style({ position: 'absolute', width: '100%', opacity: 1 }),
      animate('0.3s', style({ opacity: 0 }))],
      { optional: true },
    ),
    query(
      ':enter',
      [style({ opacity: 0 }),
      animate('0.3s', style({ opacity: 1 }))],
      { optional: true },
    ),
  ]),
]);

