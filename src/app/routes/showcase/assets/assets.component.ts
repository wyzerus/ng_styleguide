import { Component } from '@angular/core';

import { SharedService } from '../../../service/shared.service';

@Component({
  templateUrl: './assets.component.html',
})
export class AssetsComponent {

  public standardIcons: [string, string][] = Array.from(new Map<string, string>()
    .set('favicon-16x16.png', '16x16 png')
    .set('favicon-32x32.png', '32x32 png')
    .set('favicon.ico', 'ico'));

  public androidIcons: [string, string][] = Array.from(new Map<string, string>()
    .set('android-chrome-192x192.png', '192x192')
    .set('android-chrome-256x256.png', '256x256'));

  public appleIcons: [string, string][] = Array.from(new Map<string, string>()
    .set('apple-touch-icon-57x57.png', '57x57')
    .set('apple-touch-icon-60x60.png', '60x60')
    .set('apple-touch-icon-72x72.png', '72x72')
    .set('apple-touch-icon-76x76.png', '76x76')
    .set('apple-touch-icon-114x114.png', '114x114')
    .set('apple-touch-icon-120x120.png', '120x120')
    .set('apple-touch-icon-144x144.png', '144x144')
    .set('apple-touch-icon-152x152.png', '152x152')
    .set('apple-touch-icon-180x180.png', '180x180')
    .set('apple-touch-icon.png', '180x180'));

  public mstileIcons: [string, string][] = Array.from(new Map<string, string>()
    .set('mstile-150x150.png', '150x150')
    .set('mstile-310x310.png', '310x310'));

  public splashImages: [string, string][] = Array.from(new Map<string, string>()
    .set('launch_640x1136.png', '640x1136')
    .set('launch_750x1334.png', '750x1334')
    .set('launch_1125x2436.png', '1125x2436')
    .set('launch_1242x2208.png', '1242x2208')
    .set('launch_1536x2048.png', '1536x2048')
    .set('launch_1668x2224.png', '1668x2224')
    .set('launch_2048x2732.png', '2048x2732'));

  public logos: [string, string][] = Array.from(new Map<string, string>()
    .set('logo_alt.svg', 'Alt (Transparent SVG)')
    .set('logo_alt@2x.png', 'Alt 2x (Transparent PNG')
    .set('logo.svg', 'Standard (SVG)')
    .set('logo@2x.png', 'Standard 2x (PNG)'));

  constructor(public sharedService: SharedService) { }
}
