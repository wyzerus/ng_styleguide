import { Component } from '@angular/core';

import { SharedService } from '../../service/shared.service';
import { secondaryRouterFadeAnimation } from '../../animations/animations';

@Component({
  templateUrl: './showcase.component.html',
  animations: [secondaryRouterFadeAnimation],
})
export class ShowcaseComponent {

  constructor(public sharedService: SharedService) {
  }

  public menuToggle() {
    this.sharedService.menuOpen = !this.sharedService.menuOpen;
  }

}
