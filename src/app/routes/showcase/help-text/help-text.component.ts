import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  templateUrl: './help-text.component.html',
})
export class HelpTextComponent implements OnInit {

  public form: FormGroup;
  public submitted = false;

  constructor(private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      Number: ['', [
        Validators.required,
      ]],
    });
  }

  ngOnInit() {
  }

}
