import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ShowcaseComponent } from './showcase.component';
import { IconsComponent } from './icons/icons.component';
import { FormsComponent } from './forms/forms.component';
import { HelpTextComponent } from './help-text/help-text.component';
import { MessagesComponent } from './messages/messages.component';
import { UtilsComponent } from './utils/utils.component';
import { TooltipsComponent } from './tooltips/tooltips.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { HomeComponent } from './home/home.component';
import { ColorsComponent } from './colors/colors.component';
import { FontsComponent } from './fonts/fonts.component';
import { AssetsComponent } from './assets/assets.component';
import { BackToTopComponent } from './back-to-top/back-to-top.component';
import { LoadingBoxComponent } from './loading-box/loading-box.component';

const SHOWCASE_ROUTES: Routes = [
  {
    path: '', component: ShowcaseComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: 'assets', component: AssetsComponent },
      { path: 'backtop', component: BackToTopComponent },
      { path: 'loadingbox', component: LoadingBoxComponent },
      { path: 'colors', component: ColorsComponent },
      { path: 'fonts', component: FontsComponent },
      { path: 'buttons', component: ButtonsComponent },
      { path: 'icons', component: IconsComponent },
      { path: 'forms', component: FormsComponent },
      { path: 'helptext', component: HelpTextComponent },
      { path: 'messages', component: MessagesComponent },
      { path: 'tooltips', component: TooltipsComponent },
      { path: 'utils', component: UtilsComponent },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(SHOWCASE_ROUTES),
  ],
  exports: [
    RouterModule,
  ],
})
/* istanbul ignore next */
export class ShowcaseRoutingModule { }

export const routingComponents = [AssetsComponent, BackToTopComponent, ButtonsComponent, ColorsComponent, FontsComponent, FormsComponent,
  HelpTextComponent, HomeComponent, IconsComponent, LoadingBoxComponent, MessagesComponent, ShowcaseComponent, TooltipsComponent,
  UtilsComponent];
