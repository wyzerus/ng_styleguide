import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { CompleterData, CompleterService } from 'ng2-completer';
import { CustomValidators } from 'ngx-custom-validators';
import { DateTime } from 'luxon';
import {
  ReferenceKeyPair, validateValueIsInAllowedArray, validateDay, validateMonth, validateYear, validateDate, getPostcodeValidators,
  getVehicleRegValidators,
} from '@esure-dev/utils';

@Component({
  templateUrl: './forms.component.html',
})
export class FormsComponent {

  public form: FormGroup;
  public submitted: boolean = false;
  public formSuccess: boolean = false;
  public autocompleteArray: CompleterData;

  public titlesArray: ReferenceKeyPair[] = [
    { code: 'Mr', label: 'Mr' },
    { code: 'Mrs', label: 'Mrs' },
    { code: 'Miss', label: 'Miss' },
    { code: 'Ms', label: 'Ms' },
    { code: 'Dr', label: 'Dr' },
    { code: 'Reverend', label: 'Reverend' },
  ];

  private employmentStatusArray: ReferenceKeyPair[] = [
    { code: 'Employed', label: 'Employed' },
    { code: 'In Education', label: 'In Education' },
    { code: 'Self employed', label: 'Self Employed' },
    { code: 'Voluntary', label: 'Voluntary' },
    { code: 'Independent means', label: 'Independent Means' },
    { code: 'Not Employed', label: 'Not Employed' },
    { code: 'Not employed due to disability', label: 'Not Employed Due to Disability' },
    { code: 'Retired', label: 'Retired' },
    { code: 'Housewife/husband', label: 'Housewife / House Husband' },
  ];

  private maxDate: DateTime = DateTime.local().endOf('day');
  private minDate: DateTime = DateTime.local().minus({ years: 100 });

  constructor(private formBuilder: FormBuilder, private completerService: CompleterService) {
    this.autocompleteArray = this.completerService.local(this.employmentStatusArray, 'label', 'label');
    this.form = this.formBuilder.group({
      Email: ['', [
        Validators.required,
        Validators.maxLength(128),
        CustomValidators.email,
      ]],
      Password: ['', [
        Validators.required,
      ]],
      Firstname: ['', [
        Validators.required,
      ]],
      Lastname: ['', [
        Validators.required,
      ]],
      VehicleReg: ['', getVehicleRegValidators()],
      Postcode: ['', getPostcodeValidators()],
      Autocomplete: ['', [
        Validators.required,
        validateValueIsInAllowedArray(this.employmentStatusArray, 'label'),
      ]],
      Number: ['', [
        Validators.required,
      ]],
      Radios: ['', [
        Validators.required,
      ]],
      Selects: ['', [Validators.required]],
      Dates: this.formBuilder.group(
        {
          Day: ['', [
            Validators.required,
            validateDay,
          ]],
          Month: ['', [
            Validators.required,
            validateMonth,
          ]],
          Year: ['', [
            Validators.required,
            validateYear,
          ]],
        },
        {
          validator: validateDate('Day', 'Month', 'Year', this.maxDate, this.minDate),
        },
      ),
    });
  }

  public submitForm() {
    if (this.form.valid) {
      this.formSuccess = true;
    } else {
      this.submitted = true;
    }
  }

  public resetForm() {
    this.form.reset();
    this.formSuccess = false;
  }

}
