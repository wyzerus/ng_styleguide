import { Component, OnInit } from '@angular/core';
import { HttpService, HttpErrorModel, ScriptLoaderService, ScriptModel, LoggerService } from '@esure-dev/utils';
import { Message } from '@esure-dev/styles';

interface MockGetRequestResponse {
  status: string;
}

@Component({
  templateUrl: './utils.component.html',
})
export class UtilsComponent implements OnInit {

  public mockGetData: object;
  public messages: Message[] = [];
  public scriptLoadSuccess: boolean = false;

  constructor(private httpService: HttpService, private scriptLoaderService: ScriptLoaderService, private logger: LoggerService) { }

  ngOnInit() {
  }

  public triggerErrorHandler() {
    throw new Error('Manually triggerring an error!');
  }

  public addScript() {
    const model: ScriptModel = {
      name: 'MOCKJS',
      src: 'assets/mockdata/mock-js-file.js',
      loaded: false,
    };
    this.scriptLoaderService.load(model)
      .subscribe(
        () => {
          this.scriptLoadSuccess = true;
        },
        (error: any) => {
          this.logger.error('Error loading script:', error);
        },
      );
  }

  public makeHttpRequest() {
    // Clear the messages
    this.messages = [];
    this.httpService.get<MockGetRequestResponse>('assets/mockdata/mock-get-request.json')
      .subscribe(
        (data: MockGetRequestResponse) => this.mockGetData = data,
        (error: HttpErrorModel) => {
          this.messages.push({
            severity: 'danger',
            closable: true,
            heading: 'HTTP Request Error',
            summary: JSON.stringify(error),
          });
        });
  }

}
