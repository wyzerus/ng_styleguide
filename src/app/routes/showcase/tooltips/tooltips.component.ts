import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: './tooltips.component.html',
})
export class TooltipsComponent implements OnInit {

  /** For the demo page, the tooltip will initially be enabled. */
  public tooltipEnabled = true;

  constructor() { }

  ngOnInit() {
  }

}
