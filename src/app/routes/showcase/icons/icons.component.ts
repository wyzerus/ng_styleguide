import { Component, OnInit } from '@angular/core';

import { SharedService } from '../../../service/shared.service';

@Component({
  templateUrl: './icons.component.html',
})
export class IconsComponent implements OnInit {
  coreIcons: any;
  webIcons: any;

  public colorIcons: [string, string][] = Array.from(new Map<string, string>()
    .set('multicar', 'has-text-primary')
    .set('cancellation', 'has-text-prim-alt')
    .set('car', 'has-text-danger')
    .set('contents', 'has-text-info')
    .set('email', 'has-text-warning')
    .set('excess_protection', 'has-text-success'));

  public sizeIcons: [string, string][] = Array.from(new Map<string, string>()
    .set('fwd-arrow', '16x16')
    .set('make-claim', '24x24')
    .set('special-offers', '32x32')
    .set('help-support', '48x48')
    .set('log-out', '64x64')
    .set('notify-iv', '96x96')
    .set('notify', '128x128'));

  constructor(public sharedService: SharedService) {}

  ngOnInit() {
    this.coreIcons = [
      'multicar',
      'cancellation',
      'car',
      'cover',
      'breakdown',
      'breakdown-qb',
      'no_claims',
      'contents',
      'email',
      'excess_protection',
      'family_legal',
      'fridge_freezer',
      'golf',
      'home_emergency',
      'key_cover',
      'medical_expenses',
      'misfuelling',
      'motor_legal_protection',
      'ncd',
      'new_old',
      'personal_claims_handler',
      'personal_injury',
      'personal_possession',
      'pest',
      'special_event',
      'travel',
      'winter_sports',
      'alarm_clock',
      'avatar',
      'baggage_personal_money',
      'breakdown',
      'buildings',
      'business_travel',
      'call',
    ];
    this.webIcons = [
      {name: 'fwd-arrow'},
      {name: 'pdf'},
      {name: 'make-claim'},
      {name: 'special-offers'},
      {name: 'help-support'},
      {name: 'log-out'},
      {name: 'notify-iv'},
      {name: 'notify'},
      {name: 'documents'},
      {name: 'login-iv'},
      {name: 'login'},
      {name: 'burger'},
      {name: 'close'},
      {name: 'facebook'},
      {name: 'twitter'},
      {name: 'warning'},
    ];
  }
}
