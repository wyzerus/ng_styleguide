import { Component, Inject } from '@angular/core';
import { DataLayerService, LoggerService, WINDOW } from '@esure-dev/utils';

import { SharedService } from '../../../service/shared.service';

@Component({
  templateUrl: './home.component.html',
})
export class HomeComponent {

  constructor(public sharedService: SharedService, private dataLayerService: DataLayerService, private logger: LoggerService,
    @Inject(WINDOW) private win: Window) {
    this.logger.debug('Data layer initially is:', this.dataLayerService.getAll());
    this.dataLayerService.add({ cheeseProp: 'Cheese!' });
    this.logger.debug('Data layer after adding cheeseProp is:', this.dataLayerService.getAll());
    this.logger.debug('Window is: ', this.win);
  }

}
