import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HighlightJsModule } from 'ngx-highlight-js';
import { ButtonModule, HelpTextModule, GlobalMessagesModule } from '@esure-dev/styles';
import {
  AutocompleteModule, DateModule, EmailModule, FirstnameModule, LastnameModule, NumberModule, PasswordModule, RadioModule, SelectModule,
  VehicleRegModule, PostcodeModule,
} from '@esure-dev/forms';
import { CapitalizePipeModule, DateStringPipeModule, DataLayerModule, PluralPipeModule, throwIfAlreadyLoaded } from '@esure-dev/utils';

import { ShowcaseRoutingModule, routingComponents } from './showcase.routes';
import { SidebarComponent } from '../../components/sidebar/sidebar.component';
import { ViewSourceComponent } from '../../components/view-source/view-source.component';

/**
 * Showcase pages module.
 */
@NgModule({
  declarations: [
    // Components / Directives/ Pipes
    routingComponents,
    SidebarComponent,
    ViewSourceComponent,
  ],
  imports: [
    HighlightJsModule,
    ShowcaseRoutingModule,
    CommonModule,
    ReactiveFormsModule,
    AutocompleteModule,
    ButtonModule,
    DateModule,
    DataLayerModule,
    EmailModule,
    FirstnameModule,
    GlobalMessagesModule,
    HelpTextModule,
    LastnameModule,
    NumberModule,
    PasswordModule,
    RadioModule,
    SelectModule,
    VehicleRegModule,
    PostcodeModule,
    CapitalizePipeModule,
    DateStringPipeModule,
    PluralPipeModule,
  ],
})
export class ShowcaseModule {

  constructor(@Optional() @SkipSelf() parentModule: ShowcaseModule) {
    throwIfAlreadyLoaded(parentModule, 'ShowcaseModule');
  }
}
