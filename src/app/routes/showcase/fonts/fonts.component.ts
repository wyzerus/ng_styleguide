import { Component } from '@angular/core';
import { SharedService } from '../../../service/shared.service';

@Component({
  templateUrl: './fonts.component.html',
})
export class FontsComponent {

  constructor(public sharedService: SharedService) {}
}
