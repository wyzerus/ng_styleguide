import { Component, OnInit } from '@angular/core';
import { Message } from '@esure-dev/styles';

@Component({
  templateUrl: './messages.component.html',
})
export class MessagesComponent implements OnInit {

  public messages: Message[] = [];

  constructor() { }

  ngOnInit() {
  }

  public addMessage(type): void {
    switch (type) {
      case 'danger':
        this.messages.push({
          severity: 'danger',
          closable: true,
          heading: 'Danger Heading - no summary',
          summary: '',
        });
        break;
      case 'info':
        this.messages.push({
          severity: 'info',
          closable: false,
          heading: 'Info Heading',
          summary: 'Info summary text - not closable',
        });
        break;
      case 'warning':
        this.messages.push({
          severity: 'warning',
          closable: true,
          heading: 'Warning Heading',
          summary: 'Warning summary text',
        });
        break;
      case 'success':
        this.messages.push({
          severity: 'success',
          closable: true,
          heading: '',
          summary: 'Success summary text - no heading',
        });
        break;
      default:
        break;
    }
  }

  public clearMessages(): void {
    this.messages = [];
  }

}
