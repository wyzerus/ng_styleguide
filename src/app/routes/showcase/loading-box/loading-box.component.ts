import { Component } from '@angular/core';
import { LoadingBoxService } from '@esure-dev/styles';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Component({
  templateUrl: './loading-box.component.html',
})
export class LoadingBoxComponent {

  constructor(private loadingBoxService: LoadingBoxService) {
  }

  public showLoader(): void {
    this.loadingBoxService.showLoader();
    of(true).pipe(delay(1250)).subscribe(() => this.loadingBoxService.hideLoader());
  }

}
