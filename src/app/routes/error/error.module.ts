import { NgModule, Optional, SkipSelf } from '@angular/core';
import { ButtonModule } from '@esure-dev/styles';
import { throwIfAlreadyLoaded } from '@esure-dev/utils';

import { ErrorRoutingModule, routingComponents } from './error.routes';

/**
 * Error pages module.
 */
@NgModule({
  declarations: [
    // Components / Directives/ Pipes
    routingComponents,
  ],
  imports: [
    ErrorRoutingModule,
    ButtonModule,
  ],
})
export class ErrorModule {

  constructor(@Optional() @SkipSelf() parentModule: ErrorModule) {
    throwIfAlreadyLoaded(parentModule, 'ErrorModule');
  }
}
