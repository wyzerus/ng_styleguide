import { Component } from '@angular/core';

import { SharedService } from '../../service/shared.service';
import { secondaryRouterFadeAnimation } from '../../animations/animations';

@Component({
  templateUrl: './error.component.html',
  animations: [secondaryRouterFadeAnimation],
})
export class ErrorComponent {

  constructor(public sharedService: SharedService) {

  }

}
