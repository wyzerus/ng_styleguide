import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';

const brandMap: Map<string, string> = new Map<string, string>()
  .set('esure', 'esure')
  .set('sheilaswheels', 'Sheilas\' Wheels')
  .set('firstalternative', 'First Alternative');

const localStorageBrandName = 'esure-style-guide-brand';
const basePageTitle = 'Component Library Style Guide | ';
const themeStylesheetId = 'theme-css';

@Injectable()
export class SharedService {

  /** Current theme. */
  public theme: string = 'esure';

  /**
   * State of sidebar menu setting open classes on main and sidebar elements
   * @type {boolean}
   */
  public menuOpen: boolean;
  public burgerActive: boolean;

  /** Hide all source. */
  public hideAllSource: boolean = false;

  constructor(private titleService: Title) {
    const savedBrand = localStorage.getItem(localStorageBrandName);
    if (savedBrand) {
      this.changeTheme(savedBrand);
    }
  }

  public hideAllCode() {
    this.hideAllSource = !this.hideAllSource;
  }

  public changeTheme(theme: string): void {
    const themeLink: HTMLLinkElement = <HTMLLinkElement>document.getElementById(themeStylesheetId);
    themeLink.href = theme + '.css';
    this.theme = theme;
    localStorage.setItem(localStorageBrandName, theme);
    this.titleService.setTitle(basePageTitle + this.brandTitle);
  }

  get brandTitle() {
    return brandMap.get(this.theme);
  }

}
