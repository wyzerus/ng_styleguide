# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="3.1.0"></a>
# 3.1.0 (2018-11-21)

### Features

* **forms:** postcode form field component, new release v0.0.21 ([6337586](https://github.com/esure-dev/digital-app-components/commit/6337586))

### Bug Fixes

* **styles:** do not uppercase placeholder attribute in Internet Explorer, new release v0.0.39 ([3a4ec2b](https://github.com/esure-dev/digital-app-components/commit/3a4ec2b))
* **showcase:** uncomment browser polyfills to allow app to work in Internet Explorer ([c354738](https://github.com/esure-dev/digital-app-components/commit/c354738))

<a name="3.0.0"></a>
# 3.0.0 (2018-11-20)

### Features

* **forms:** left icon in number field component ([#25](https://github.com/esure-dev/digital-app-components/issues/25)) ([5c40a0c](https://github.com/esure-dev/digital-app-components/commit/5c40a0c))
* **styles:** update icons, new version of styles lib v0.0.37 ([#23](https://github.com/esure-dev/digital-app-components/issues/23)) ([e67d1c2](https://github.com/esure-dev/digital-app-components/commit/e67d1c2))
* **utils:** plural pipe, new version of utils lib  v0.0.20 ([6a508b4](https://github.com/esure-dev/digital-app-components/commit/6a508b4))
* module exported for lib & demos added to showcase page / app ([69fdeb2](https://github.com/esure-dev/digital-app-components/commit/69fdeb2))
* module exported for lib & demos added to showcase page / app ([b133674](https://github.com/esure-dev/digital-app-components/commit/b133674))
* new date string pipe added with tests ([70da36b](https://github.com/esure-dev/digital-app-components/commit/70da36b))


<a name="1.5.0"></a>
# 1.5.0 (2018-10-14)

### Features

* **styles:** loading box service ([c01fc07](https://github.com/esure-dev/digital-app-components/commit/c01fc07))

<a name="1.4.0"></a>
# 1.4.0 (2018-09-28)

### Features

* extract portal breakpoints ([2f1f5ce](https://github.com/esure-dev/digital-app-components/commit/2f1f5ce))

<a name="1.3.0"></a>
# 1.3.0 (2018-09-27)

### Features

* **utils:** data layer service ([d4cfd3f](https://github.com/esure-dev/digital-app-components/commit/d4cfd3f))

<a name="1.2.0"></a>
# 1.2.0 (2018-09-26)

### Features

* **utils:** platform agnostic browser window object ([6b9d58f](https://github.com/esure-dev/digital-app-components/commit/6b9d58f))

<a name="1.1.0"></a>
# 1.1.0 (2018-09-26)

### Features

* **utils:** logger service ([d1eff6f](https://github.com/esure-dev/digital-app-components/commit/d1eff6f))

<a name="1.0.0"></a>
# 1.0.0 (2018-09-25)

* Optimize utils (#10) ([6fc4919](https://github.com/esure-dev/digital-app-components/commit/6fc4919)), closes [#10](https://github.com/esure-dev/digital-app-components/issues/10)

### Performance Improvements

* refactor utils into single exported functions for tree shaking ([6aba37c](https://github.com/esure-dev/digital-app-components/commit/6aba37c))

### BREAKING CHANGES

* util functions now need to be individually imported

<a name="0.8.0"></a>
# 0.8.0 (2018-09-24)

### Features

* added back to top page and logos to assets page ([8722675](https://github.com/esure-dev/digital-app-components/commit/8722675))
* back to top implemented ([e11455a](https://github.com/esure-dev/digital-app-components/commit/e11455a))
* hover background color ([380507e](https://github.com/esure-dev/digital-app-components/commit/380507e))
* start of back-to-top component ([7ba03d0](https://github.com/esure-dev/digital-app-components/commit/7ba03d0))
* use observable interval instead of window setInterval ([20b2e14](https://github.com/esure-dev/digital-app-components/commit/20b2e14))

<a name="0.7.0"></a>
# 0.7.0 (2018-09-21)

### Features

* router transition animations ([6d61217](https://github.com/esure-dev/digital-app-components/commit/6d61217))

<a name="0.6.0"></a>
# 0.6.0 (2018-09-20)

### Features

* enable anchor scrolling ([5232508](https://github.com/esure-dev/digital-app-components/commit/5232508))

<a name="0.5.0"></a>
# 0.5.0 (2018-09-19)

### Features

* loader before app is bootstrapped ([b038421](https://github.com/esure-dev/digital-app-components/commit/b038421))

<a name="0.4.1"></a>
## 0.4.1 (2018-09-19)

### Bug Fixes

* prevent text showing infront of logo on error pages ([46e94df](https://github.com/esure-dev/digital-app-components/commit/46e94df))

<a name="0.4.0"></a>
# 0.4.0 (2018-09-19)

### Features

* **styles:** added brand logos to assets ([709f370](https://github.com/esure-dev/digital-app-components/commit/709f370))

<a name="0.3.1"></a>
## 0.3.1 (2018-09-17)

### Bug Fixes

* remove compare links as tags for those versions do not exist ([56350b2](https://github.com/esure-dev/digital-app-components/commit/56350b2))

<a name="0.3.0"></a>
# 0.3.0 (2018-09-17)

### Features

* **linting:** add commitlint to lint commit messages ([0155fb3](https://github.com/esure-dev/digital-app-components/commit/0155fb3))

<a name="0.2.0"></a>
# 0.2.0 (2018-09-17)

### Features

* do not create a tag on release ([5becd55](https://github.com/esure-dev/digital-app-components/commit/5becd55))
* ignore pre commit hooks on release ([ff348b7](https://github.com/esure-dev/digital-app-components/commit/ff348b7))

<a name="0.1.0"></a>
# 0.1.0 (2018-09-17)

### Features

* do not create a tag on release ([5becd55](https://github.com/esure-dev/digital-app-components/commit/5becd55))

<a name="0.0.2"></a>
## 0.0.2 (2018-09-17)

<a name="0.0.1"></a>
## 0.0.1 (2018-09-17)

### Features

* added standard-version library for changelog generation ([4b3103f](https://github.com/esure-dev/digital-app-components/commit/4b3103f))
