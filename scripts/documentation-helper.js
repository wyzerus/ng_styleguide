/* eslint-disable no-console */

// This script is used to copy the static documentation reports into a centralised documentation folder
const fs = require('fs-extra');
const path = require('path');

const outputDirFolderName = 'documentation';

console.log('Emptying ' + outputDirFolderName + ' folder...');
fs.emptyDirSync(outputDirFolderName);

const showcaseAppFolder = 'dist/showcase';
const compodocFolder = 'compodoc';
const testReportsFolder = 'test-reports';

const showcaseAppFullPath = path.resolve(__dirname, '../' + showcaseAppFolder);
const showcaseAppOutputFullPath = path.resolve(__dirname, '../' + outputDirFolderName + '/' + showcaseAppFolder);
const compodocFullPath = path.resolve(__dirname, '../' + compodocFolder);
const compodocOutputFullPath = path.resolve(__dirname, '../' + outputDirFolderName + '/' + compodocFolder);
const testReportsFullPath = path.resolve(__dirname, '../' + testReportsFolder);
const testReportsOutputFullPath = path.resolve(__dirname, '../' + outputDirFolderName + '/' + testReportsFolder);

console.log('Copying ' + showcaseAppFolder + ' folder to ' + outputDirFolderName + '...');
fs.copySync(showcaseAppFullPath, showcaseAppOutputFullPath);

console.log('Copying ' + compodocFolder + ' folder to ' + outputDirFolderName + '...');
fs.copySync(compodocFullPath, compodocOutputFullPath);

console.log('Copying ' + testReportsFolder + ' folder to ' + outputDirFolderName + '...');
fs.copySync(testReportsFullPath, testReportsOutputFullPath);
