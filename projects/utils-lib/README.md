# Esure Utils Library

Utils library providing the following:

* Error Handler
* Routes Preloader
* Http Module
* Logger Module
* Window Module
* DataLayer Module
* Script Loader Service
* Pipes
* Directives
* Utility Functions
