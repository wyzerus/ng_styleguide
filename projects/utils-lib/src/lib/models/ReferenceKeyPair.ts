/**
 * This is essentially a key and value pairing, typically used for the arrays for populating select boxes.
 *
 * @export
 */
export interface ReferenceKeyPair {
  code: string;
  label: string;
}

/**
 * The possible property types that exist on ReferenceKeyPair interface.
 * @export
 */
export type RefKeyPairPropType = 'code' | 'label';
