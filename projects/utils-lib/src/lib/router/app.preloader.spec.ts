import { Route } from '@angular/router';
import { Observable, of } from 'rxjs';

import { AppPreloader } from './app.preloader';

describe('AppPreloader', () => {

  const appPreloader: AppPreloader = new AppPreloader();

  it('should return observalbe of null if route has no data on it', (done) => {
    const route: Route = {
    };
    const result: Observable<any> = appPreloader.preload(route, null);
    result.subscribe((data: any) => {
      expect(data).toBeNull();
      done();
    });
  });

  it('should return observalbe of null if route has data on it but preload is false', (done) => {
    const route: Route = {
      data: {
        preload: false,
      },
    };
    const result: Observable<any> = appPreloader.preload(route, null);
    result.subscribe((data: any) => {
      expect(data).toBeNull();
      done();
    });
  });

  it('should call the load() function if preload is true', (done) => {
    const route: Route = {
      data: {
        preload: true,
      },
    };
    const load = () => of('loaded');
    const result: Observable<any> = appPreloader.preload(route, load);
    result.subscribe((data: any) => {
      expect(data).toEqual('loaded');
      done();
    });
  });

});
