import { PreloadingStrategy, Route } from '@angular/router';
import { Observable, of } from 'rxjs';

/**
 * Custom pre loading strategy. The default for lazy loaded routes is to lazy load when route is requested.
 * If you want to load in the background, then set a data attribute on the route with preload set to true.
 *
 * @export
 */
export class AppPreloader implements PreloadingStrategy {
  preload(route: Route, load: Function): Observable<any> {
    return route.data && route.data.preload ? load() : of(null);
  }
}
