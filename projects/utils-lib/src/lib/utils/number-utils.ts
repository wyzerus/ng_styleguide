// Number utils helper functions.

/**
 * Pad a number with leading zeros if required, if the params 1, 2 are passed in, the final output will be the string 01.
 * @param num the number to add leading zeros to if required
 * @param size the final length the number should be
 */
export function pad(num, size): string {
  let s = num + '';
  while (s.length < size) {
    s = '0' + s;
  }
  return s;
}
