// Validator utils helper functions.

import { AbstractControl, ValidationErrors, ValidatorFn, Validators, FormGroup } from '@angular/forms';
import { DateTime, Duration, DurationObject } from 'luxon';
import { CustomValidators } from 'ngx-custom-validators';

import { ReferenceKeyPair, RefKeyPairPropType } from '../models/ReferenceKeyPair';
import { pad } from './number-utils';

const DATE_DAY_REGEX = new RegExp('^(0?[1-9]|[12][0-9]|3[01])$');
const DATE_MONTH_REGEX = new RegExp('^(0?[1-9]|1[012])$');
const DATE_YEAR_REGEX = new RegExp('^(19|20)[0-9]{2}$');
const POSTCODE_REGEX = new RegExp('^\\s*[A-Za-z]{1,2}([0-9]{1,2}|[0-9][A-Za-z])\\s*[0-9][A-Za-z]{2}\\s*$', 'i');
const VEHICLE_REG_REGEX = new RegExp('^((\\s*[0-9]{1}\\s*[A-Z]{1}\\s*)|(\\s*[A-Z]){1,3}(\\s*[0-9]){1,3}(\\s*[A-Z]){0,3}\\s*)$', 'i');

/**
 * The possible date field control names.
 * @export
 */
export type DateFieldControlName = 'Day' | 'Month' | 'Year';

/**
 * The possible values for beforeOrAfter property of ValidateAgainstDate.
 * @export
 */
export type ValidateAgainstDateBeforeOrAfter = 'before' | 'after';

/**
 * Contains information to be used to validate a date against the value of another date.
 * An example: Value of dateFormGroup passed in is 01/01/2018 and a duration of 1 year is passed in and beforeOrAfter set to after.
 * This means a date of 31/12/2017 is invalid, and a date of 02/01/2019 is invalid, anything inbetween is valid.
 *
 * @export
 */
export interface ValidateAgainstDate {
  dateFormGroup: FormGroup;
  dayKey: string;
  monthKey: string;
  yearKey: string;
  duration: Duration | number | DurationObject;
  /**
   * The specified duration will either be added to (if after) or subtracted from (if before) the dateFormGroup. If
   * after is passed in, the date must be greater than the dateFormGroup value, and less than the dateFormGroup value with
   * the duration added to it. If before is passed in, the date must be less than the dateFormGroup value, but more than the
   * dateFormGroup value with the duration subtracted from it.
   *
   * @memberof ValidateAgainstDate
   */
  beforeOrAfter: ValidateAgainstDateBeforeOrAfter;
}

/**
 * Validates a date. Can validate against a min and max, as well as against the value of another form input date.
 *
 * @param dayKey
 * @param monthKey
 * @param yearKey
 * @param [maxDate]
 * @param [minDate]
 * @param [againstDate]
 * @returns ValidatorFn
 */
export function validateDate(
  dayKey: string, monthKey: string, yearKey: string, maxDate?: DateTime, minDate?: DateTime,
  againstDate?: ValidateAgainstDate): ValidatorFn {
  let subscribed = false;
  return (group: FormGroup): ValidationErrors | null => {
    // Initially set it up to return null, as returning null marks the FormGroup as valid
    let returnObject = null;
    const day = group.controls[dayKey];
    const month = group.controls[monthKey];
    const year = group.controls[yearKey];

    // Only do extra validation if all 3 fields are themselves valid
    if (day.valid && month.valid && year.valid) {

      const dateStr = `${year.value}-${pad(month.value, 2)}-${pad(day.value, 2)}`;
      const luxonDate: DateTime = DateTime.fromISO(dateStr);
      if (!luxonDate.isValid) {
        returnObject = {
          invalidDate: true,
        };
      } else if (maxDate && luxonDate > maxDate) {
        returnObject = {
          dateAfter: true,
        };
      } else if (minDate && luxonDate < minDate) {
        returnObject = {
          dateBefore: true,
        };
      } else if (againstDate) {
        if (!subscribed) {
          // Subscribe to changes on the other date form group to update the validity on this date group, only do this once
          subscribed = true;
          againstDate.dateFormGroup.valueChanges.subscribe(() => {
            group.updateValueAndValidity();
          });
        }

        if (againstDate.dateFormGroup.valid) {
          const againstDay = againstDate.dateFormGroup.controls[dayKey];
          const againstMonth = againstDate.dateFormGroup.controls[monthKey];
          const againstYear = againstDate.dateFormGroup.controls[yearKey];
          const againstDateStr = `${againstYear.value}-${pad(againstMonth.value, 2)}-${pad(againstDay.value, 2)}`;
          const againstMomentDate = DateTime.fromISO(againstDateStr);

          if (againstDate.beforeOrAfter === 'before') {
            const minusDate = againstMomentDate.minus(againstDate.duration);
            if (luxonDate < minusDate || luxonDate > againstMomentDate) {
              returnObject = {
                dateInvalidAgainstOther: true,
              };
            }
          } else {
            const plusDate = againstMomentDate.plus(againstDate.duration);
            if (luxonDate > plusDate || luxonDate < againstMomentDate) {
              returnObject = {
                dateInvalidAgainstOther: true,
              };
            }
          }
        }
      }
    } else {
      returnObject = {
        allFieldsNotValid: true,
      };
    }

    return returnObject;
  };
}

/**
 * Validates the day field of a date.
 *
 * @param control
 * @returns ValidationErrors or null if passed validation
 */
export function validateDay(control: AbstractControl): ValidationErrors | null {
  if (!DATE_DAY_REGEX.test(control.value)) {
    return { validateDay: true };
  }
  return null;
}

/**
 * Validates the month field of a date.
 *
 * @param control
 * @returns ValidationErrors or null if passed validation
 */
export function validateMonth(control: AbstractControl): ValidationErrors | null {
  if (!DATE_MONTH_REGEX.test(control.value)) {
    return { validateMonth: true };
  }
  return null;
}

/**
 * Validates the year field of a date.
 *
 * @param control
 * @returns ValidationErrors or null if passed validation
 */
export function validateYear(control: AbstractControl): ValidationErrors | null {
  if (!DATE_YEAR_REGEX.test(control.value)) {
    return { validateYear: true };
  }
  return null;
}

/**
 * Gets the date validators to use for a day, month, or year form field.
 * It assumes that the key names they have been given in the FormGroup is Day, Month and Year.
 *
 * @param controlName
 * @returns ValidatorFn[]
 */
export function getDateFieldValidators(controlName: DateFieldControlName): ValidatorFn[] {
  switch (controlName) {
    case 'Day':
      return [Validators.required, validateDay];
    case 'Month':
      return [Validators.required, validateMonth];
    case 'Year':
      return [Validators.required, validateYear];
    default:
      throw Error('Unexpected control name passed to getDateFieldValidators: ' + controlName);
  }
}

/**
 * Get the validators to use for a postcode field.
 *
 * @returns ValidatorFn[]
 */
export function getPostcodeValidators(): ValidatorFn[] {
  return [Validators.required, CustomValidators.rangeLength([2, 8]), Validators.pattern(POSTCODE_REGEX)];
}

/**
 * Get the validators to use for a vehicle registration field.
 *
 * @returns ValidatorFn[]
 */
export function getVehicleRegValidators(): ValidatorFn[] {
  return [Validators.required, CustomValidators.rangeLength([2, 8]), Validators.pattern(VEHICLE_REG_REGEX)];
}

/**
 * Checks the value exists in the allowed array.
 *
 * @param allowedArray
 * @param prop which property to validate against, defaults to code, but could be label if passed in
 * @returns ValidatorFn
 */
export function validateValueIsInAllowedArray(allowedArray: ReferenceKeyPair[], prop: RefKeyPairPropType = 'code'): ValidatorFn {
  const result = (control: AbstractControl): ValidationErrors | null => {
    const valid = allowedArray.some((element: ReferenceKeyPair) => element[prop] === control.value);
    return valid ? null : {
      invalidValue: true,
    };
  };
  return result;
}
