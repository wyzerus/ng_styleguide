// String utils helper functions.

const ALPHA_REGEX = new RegExp('[^a-z]*', 'g');
const WHITESPACE_REGEX = new RegExp('\\s', 'g');
const SPACE_REGEX = new RegExp(' ', 'g');

/**
 * Sanitizes an input by removing any non alpha characters, including whitespace and making the input lowercase.
 *
 * @param str
 * @returns sanitized string
 */
export function sanitizeToAlpha(str: string): string {
  return str ? str.toLowerCase().replace(ALPHA_REGEX, '') : str;
}

/**
 * Removes all whitespace and uppercases the string.
 *
 * @param str
 * @returns uppercased and no whitespace string
 */
export function removeWhitespaceAndMakeUppercase(str: string): string {
  return str ? str.toUpperCase().replace(WHITESPACE_REGEX, '') : str;
}

/**
 * Replaces any whitespace with the specified replacement char.
 *
 * @param str
 * @param replacementChar
 * @returns the string with whitespace replaced by the specified character
 */
export function replaceWhiteSpaceWithChar(str: string, replacementChar: string): string {
  return str ? str.replace(SPACE_REGEX, replacementChar) : str;
}

/**
 * Removes a slash from the start of a string if present.
 *
 * @param str
 * @returns the string with start slash removed if it was present
 */
export function removeStartSlash(str: string): string {
  return str.startsWith('/') ? str.slice(1) : str;
}

/**
 * Adds a slash to the end of a string if not already present.
 *
 * @param str
 * @returns the string with the slash at the end if it was not already present
 */
export function addEndSlash(str: string): string {
  return str.endsWith('/') ? str : `${str}/`;
}
