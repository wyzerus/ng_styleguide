import { AbstractControl, FormControl, ValidationErrors, ValidatorFn, FormGroup, Validators } from '@angular/forms';
import { DateTime } from 'luxon';

import { ReferenceKeyPair } from '../models/ReferenceKeyPair';
import * as ValidatorUtils from './validator-utils';


describe('ValidatorUtils', () => {

  describe('validateYear', () => {
    it('should return null for valid years', () => {
      const control: AbstractControl = new FormControl('2000');
      const result: ValidationErrors | null = ValidatorUtils.validateYear(control);
      expect(result).toBeNull();
    });

    it('should return an error for invalid years', () => {
      const control: AbstractControl = new FormControl('1850');
      const result: ValidationErrors | null = ValidatorUtils.validateYear(control);
      expect(result).toEqual({ validateYear: true });
    });
  });

  describe('validateMonth', () => {
    it('should return null for valid months', () => {
      const control: AbstractControl = new FormControl('12');
      const result: ValidationErrors | null = ValidatorUtils.validateMonth(control);
      expect(result).toBeNull();
    });

    it('should return an error for invalid months', () => {
      const control: AbstractControl = new FormControl('13');
      const result: ValidationErrors | null = ValidatorUtils.validateMonth(control);
      expect(result).toEqual({ validateMonth: true });
    });
  });

  describe('validateDay', () => {
    it('should return null for valid days', () => {
      const control: AbstractControl = new FormControl('25');
      const result: ValidationErrors | null = ValidatorUtils.validateDay(control);
      expect(result).toBeNull();
    });

    it('should return an error for invalid days', () => {
      const control: AbstractControl = new FormControl('32');
      const result: ValidationErrors | null = ValidatorUtils.validateDay(control);
      expect(result).toEqual({ validateDay: true });
    });
  });

  describe('getDateFieldValidators', () => {
    it('should return array if Day passed in', () => {
      const result: ValidatorFn[] = ValidatorUtils.getDateFieldValidators('Day');
      expect(result.length).toBe(2);
    });

    it('should return array if Month passed in', () => {
      const result: ValidatorFn[] = ValidatorUtils.getDateFieldValidators('Month');
      expect(result.length).toBe(2);
    });

    it('should return array if Year passed in', () => {
      const result: ValidatorFn[] = ValidatorUtils.getDateFieldValidators('Year');
      expect(result.length).toBe(2);
    });

    it('should throw an Error if null value passed in', () => {
      expect(() => ValidatorUtils.getDateFieldValidators(null)).toThrowError();
    });
  });

  describe('validateValueIsInAllowedArray', () => {
    it('should return null for a valid value', () => {
      const arr: ReferenceKeyPair[] = [{
        code: 'code',
        label: 'label',
      }];

      const control: AbstractControl = new FormControl('code');
      const result: ValidationErrors | null = ValidatorUtils.validateValueIsInAllowedArray(arr)(control);
      expect(result).toBeNull();
    });

    it('should return an error for an invalid value', () => {
      const arr: ReferenceKeyPair[] = [{
        code: 'code',
        label: 'label',
      }];

      const control: AbstractControl = new FormControl('notfound');
      const result: ValidationErrors | null = ValidatorUtils.validateValueIsInAllowedArray(arr, 'label')(control);
      expect(result).toEqual({ invalidValue: true });
    });
  });

  describe('validateDate', () => {
    it('should return an error of invalidDate for a non valid date', () => {
      const day: AbstractControl = new FormControl('33');
      const month: AbstractControl = new FormControl('06');
      const year: AbstractControl = new FormControl('2000');
      const formGroup: FormGroup = new FormGroup({
        'Day': day,
        'Month': month,
        'Year': year,
      });
      const result: ValidationErrors | null = ValidatorUtils.validateDate('Day', 'Month', 'Year')(formGroup);
      expect(result).toEqual({ invalidDate: true });
    });

    it('should return an error of allFieldsNotValid if not all fields were valid', () => {
      const day: AbstractControl = new FormControl('', [Validators.required]);
      const month: AbstractControl = new FormControl('06');
      const year: AbstractControl = new FormControl('2000');
      const formGroup: FormGroup = new FormGroup({
        'Day': day,
        'Month': month,
        'Year': year,
      });
      const result: ValidationErrors | null = ValidatorUtils.validateDate('Day', 'Month', 'Year')(formGroup);
      expect(result).toEqual({ allFieldsNotValid: true });
    });

    it('should return an error of dateAfter if date is after the maxDate', () => {
      const day: AbstractControl = new FormControl('15');
      const month: AbstractControl = new FormControl('06');
      const year: AbstractControl = new FormControl('2000');
      const formGroup: FormGroup = new FormGroup({
        'Day': day,
        'Month': month,
        'Year': year,
      });
      const maxDate: DateTime = DateTime.local().set({ 'year': 1999 });
      const result: ValidationErrors | null = ValidatorUtils.validateDate('Day', 'Month', 'Year', maxDate)(formGroup);
      expect(result).toEqual({ dateAfter: true });
    });

    it('should return an error of dateBefore if date is before the minDate', () => {
      const day: AbstractControl = new FormControl('15');
      const month: AbstractControl = new FormControl('06');
      const year: AbstractControl = new FormControl('2000');
      const formGroup: FormGroup = new FormGroup({
        'Day': day,
        'Month': month,
        'Year': year,
      });
      const minDate: DateTime = DateTime.local().set({ 'year': 2001 });
      const result: ValidationErrors | null = ValidatorUtils.validateDate('Day', 'Month', 'Year', null, minDate)(formGroup);
      expect(result).toEqual({ dateBefore: true });
    });

    it('should return an error of dateInvalidAgainstOther if date is before the other date', () => {
      const day: AbstractControl = new FormControl('15');
      const month: AbstractControl = new FormControl('06');
      const year: AbstractControl = new FormControl('2000');
      const formGroup: FormGroup = new FormGroup({
        'Day': day,
        'Month': month,
        'Year': year,
      });

      // Set the other date to be 6 days ahead, as we are going to go back 5 days from it
      const otherDateDay: AbstractControl = new FormControl('21');
      const otherDateMonth: AbstractControl = new FormControl('06');
      const otherDateYear: AbstractControl = new FormControl('2000');
      const otherDateFormGroup: FormGroup = new FormGroup({
        'Day': otherDateDay,
        'Month': otherDateMonth,
        'Year': otherDateYear,
      });
      const otherDate: ValidatorUtils.ValidateAgainstDate = {
        dateFormGroup: otherDateFormGroup,
        dayKey: 'Day',
        monthKey: 'Month',
        yearKey: 'Year',
        duration: {
          days: 5,
        },
        beforeOrAfter: 'before',
      };

      const result: ValidationErrors | null = ValidatorUtils.validateDate('Day', 'Month', 'Year', null, null, otherDate)(formGroup);
      expect(result).toEqual({ dateInvalidAgainstOther: true });
    });

    it('should return an error of dateInvalidAgainstOther if date is after the other date', () => {
      const day: AbstractControl = new FormControl('15');
      const month: AbstractControl = new FormControl('06');
      const year: AbstractControl = new FormControl('2000');
      const formGroup: FormGroup = new FormGroup({
        'Day': day,
        'Month': month,
        'Year': year,
      });

      // Set the other date to be 6 days behind, as we are going to add 5 days to it
      const otherDateDay: AbstractControl = new FormControl('09');
      const otherDateMonth: AbstractControl = new FormControl('06');
      const otherDateYear: AbstractControl = new FormControl('2000');
      const otherDateFormGroup: FormGroup = new FormGroup({
        'Day': otherDateDay,
        'Month': otherDateMonth,
        'Year': otherDateYear,
      });
      const otherDate: ValidatorUtils.ValidateAgainstDate = {
        dateFormGroup: otherDateFormGroup,
        dayKey: 'Day',
        monthKey: 'Month',
        yearKey: 'Year',
        duration: {
          days: 5,
        },
        beforeOrAfter: 'after',
      };

      const result: ValidationErrors | null = ValidatorUtils.validateDate('Day', 'Month', 'Year', null, null, otherDate)(formGroup);
      expect(result).toEqual({ dateInvalidAgainstOther: true });
    });

    it('should return null for a valid date', () => {
      const day: AbstractControl = new FormControl('15');
      const month: AbstractControl = new FormControl('06');
      const year: AbstractControl = new FormControl('2000');
      const formGroup: FormGroup = new FormGroup({
        'Day': day,
        'Month': month,
        'Year': year,
      });
      const result: ValidationErrors | null = ValidatorUtils.validateDate('Day', 'Month', 'Year')(formGroup);
      expect(result).toBeNull();
    });
  });

});
