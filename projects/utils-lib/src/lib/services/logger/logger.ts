// tslint:disable:no-console
import { Injectable, InjectionToken, Inject, Optional, NgModule, SkipSelf, ModuleWithProviders } from '@angular/core';

import { throwIfAlreadyLoaded } from '../../guards/module-import.guard';

export const LOGGER_MODULE_OPTIONS = new InjectionToken<LoggerModuleOptions>('LOGGER_MODULE_OPTIONS');

/**
 * Interface for the options the logger service takes in the constructor.
 */
export interface LoggerModuleOptions {
  /**
   * Whether it is a production build or not.
   *
   * @memberof LoggerOptions
   */
  isProduction: boolean;
}

/**
 * Logger service, basically a wrapper for the console that will only log errors in production.
 *
 * @export
 */
@Injectable({
  providedIn: 'root',
})
export class LoggerService {

  private production: boolean;

  constructor(@Optional() @Inject(LOGGER_MODULE_OPTIONS) config: LoggerModuleOptions) {
    // Assume production true if not passed in
    this.production = config ? config.isProduction : true;
  }

  debug(value: any, ...rest: any[]) {
    if (!this.production) {
      console.debug(`DEBUG: ${value}`, ...rest);
    }
  }

  info(value: any, ...rest: any[]) {
    if (!this.production) {
      console.info(`INFO: ${value}`, ...rest);
    }
  }

  log(value: any, ...rest: any[]) {
    if (!this.production) {
      console.log(`LOG: ${value}`, ...rest);
    }
  }

  warn(value: any, ...rest: any[]) {
    if (!this.production) {
      console.warn(`WARN: ${value}`, ...rest);
    }
  }

  error(value: any, ...rest: any[]) {
    console.error(`ERROR: ${value}`, ...rest);
  }

}

@NgModule()
export class LoggerModule {

  constructor(@Optional() @SkipSelf() parentModule: LoggerModule) {
    throwIfAlreadyLoaded(parentModule, 'LoggerModule');
  }

  static forRoot(options: LoggerModuleOptions): ModuleWithProviders {
    return {
      ngModule: LoggerModule,
      providers: [
        {
          provide: LOGGER_MODULE_OPTIONS,
          useValue: options,
        },
      ],
    };
  }
}
