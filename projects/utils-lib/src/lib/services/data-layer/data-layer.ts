import { Injectable, Inject, NgModule, Component, OnInit, Input } from '@angular/core';

import { WINDOW } from '../window/window';
import { LoggerService } from '../logger/logger';

/**
 * @dynamic
 * Data layer service.
 *
 * @export
 */
@Injectable({
  providedIn: 'root',
})
export class DataLayerService {

  private dataLayer: object;

  constructor(@Inject(WINDOW) private win: Window) {
    this.dataLayer = this.win['esureDataLayer'];
  }

  /**
   * Add the passed in key/value pairs to the data layer.
   *
   * @param varsToAdd object of key/value pairs to add
   */
  public add(varsToAdd: object): void {
    Object.keys(varsToAdd).forEach((prop) => {
      this.dataLayer[prop] = varsToAdd[prop];
    });
  }

  /**
   * Removes the passed in properties from the data layer.
   *
   * @param varsToRemove string array of vars to remove
   */
  public remove(varsToRemove: string[]): void {
    for (const prop of varsToRemove) {
      delete this.dataLayer[prop];
    }
  }

  public getAll(): object {
    return this.dataLayer;
  }

}

/**
 * This is responsible for updating the esureDataLayer global Javascript variable.
 */
@Component({
  selector: 'es-data-layer',
  template: '',
})
export class DataLayerComponent implements OnInit {

  @Input()
  public vars: object;

  constructor(private dataLayerService: DataLayerService, private logger: LoggerService) {
  }

  /**
   * Sets the passed in object key/value pairs onto the esureDataLayer object.
   */
  ngOnInit(): void {
    if (this.vars !== undefined) {
      this.dataLayerService.add(this.vars);
      this.logger.debug('Data layer after ngOnInit is: ', this.dataLayerService.getAll());
    }
  }
}

/**
 * Data layer module.
 *
 * @export
 */
@NgModule({
  exports: [DataLayerComponent],
  declarations: [DataLayerComponent],
})
export class DataLayerModule {
}
