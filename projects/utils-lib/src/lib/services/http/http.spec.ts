import { HttpClient, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { HttpService, HttpModule, HttpModuleOptions } from './http';

describe('Http', () => {

  describe('HttpModule passing empty options object to forRoot', () => {
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [
          HttpModule.forRoot({}),
          HttpClientTestingModule,
        ],
        providers: [
        ],
      });
    });

    it('should provide services', () => {
      expect(TestBed.get(HttpService)).toBeObject();
    });
  });

  describe('HttpModule passing an object to forRoot', () => {
    const moduleOptions: HttpModuleOptions = {
      config: {
        xapiUrl: '/xapiurl',
      },
    };

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [
          HttpModule.forRoot(moduleOptions),
          HttpClientTestingModule,
        ],
      });
    });

    it('should provide services', () => {
      expect(TestBed.get(HttpService)).toBeObject();
    });
  });

  describe('HttpModule loaded twice', () => {
    it('should throw an error if parent is not null', () => {
      // tslint:disable:no-unused-expression
      expect(() => { new HttpModule(new HttpModule(null)); }).toThrowError();
    });
  });

  describe('HttpService', () => {

    let httpService: HttpService;
    let httpClient: HttpClient;

    beforeAll(() => {
      httpClient = new HttpClient(null);
      httpService = new HttpService(httpClient, null);
    });

    it('should create the service', () => {
      expect(httpService instanceof HttpService).toBeTrue();
    });

    it('should get', () => {
      const returnVal = 'OK';
      spyOn(httpClient, 'get').and.returnValue(of(returnVal));
      const get = httpService.get('url');
      get.subscribe(
        (data) => {
          expect(data).toEqual(returnVal);
        },
        () => {
          fail('Not expected to fail');
        });
    });

    it('should getBinary', () => {
      const returnVal = new HttpResponse({ body: new Blob() });
      spyOn(httpClient, 'get').and.returnValue(of(returnVal));
      const get = httpService.getBinary('url');
      get.subscribe(
        (data) => {
          expect(data).toEqual(returnVal);
        },
        () => {
          fail('Not expected to fail');
        });
    });

    it('should post', () => {
      const returnVal = 'OK';
      spyOn(httpClient, 'post').and.returnValue(of(returnVal));
      const post = httpService.post('url', {});
      post.subscribe(
        (data) => {
          expect(data).toEqual(returnVal);
        },
        () => {
          fail('Not expected to fail');
        });
    });
  });

});
