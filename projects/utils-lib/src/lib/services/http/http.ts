import { ModuleWithProviders, NgModule, Optional, SkipSelf, InjectionToken, Inject, Provider } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { throwIfAlreadyLoaded } from '../../guards/module-import.guard';
import { addEndSlash, removeStartSlash } from '../../utils/string-utils';

export const HTTP_MODULE_CONFIG = new InjectionToken<HttpModuleConfig>('HTTP_MODULE_CONFIG');

export interface HttpModuleConfig {
  /**
     * The URL for the xAPI, if left empty will get defaulted to '/'. The passed in URL will get a trailing slash appended if not
     * already there.
     */
  xapiUrl?: string;
}

/**
 * Interface for the options this module takes when being imported into the app using forRoot() method.
 */
export interface HttpModuleOptions {

  /**
   * Provider to use for obtaining the module options.
   * @memberof HttpModuleOptions
   */
  httpOptionsProvider?: Provider;

  /**
   * The config for the http module.
   *
   * @memberof HttpModuleOptions
   */
  config?: HttpModuleConfig;
}

/**
 * Interface to describe the object that will be returned when an error occurs during any HTTP calls.
 */
export interface HttpErrorModel {
  errMsg: string;
  body: string | object;
  statusCode: number | string;
  statusText: string;
}

/**
 * Http service, used for making Http requests. Interceptors can modify the requests/responses.
 * We have our own one, HttpModifyInterceptor, and also a third party one, provided by the use of
 * including JwtModule in the main app. This JwtModule is responsible for sending the Authorisation
 * header with the Bearer and JWT token.
 */
export class HttpService {

  /**
   * The xAPI URL.
   *
   * @memberof HttpService
   */
  private xapiUrl: string;

  /**
   * Constructor that injects in the HttpClient required for making Http requests.
   * @param http built in angular http module
   */
  constructor(private http: HttpClient, @Optional() @Inject(HTTP_MODULE_CONFIG) config: HttpModuleConfig) {
    this.xapiUrl = config && config.xapiUrl ? addEndSlash(config.xapiUrl) : '/';
  }

  public get<T>(url: string): Observable<T | HttpErrorModel> {
    const fullUrl = `${this.xapiUrl}${removeStartSlash(url)}`;
    return this.http.get<T>(fullUrl, { withCredentials: true }).pipe(
      catchError(this.handleError));
  }

  /**
   * Used for getting binary data like a PDF document, the full response is returned so that the information in the headers can be used.
   * The response body is expected to be a Blob type.
   * @param url
   * @returns Observable of the Blob response or an HttpErrorModel
   * @memberof HttpService
   */
  public getBinary(url: string): Observable<HttpResponse<Blob> | HttpErrorModel> {
    const fullUrl = `${this.xapiUrl}${removeStartSlash(url)}`;
    return this.http.get(fullUrl, { withCredentials: true, responseType: 'blob', observe: 'response' }).pipe(
      catchError(this.handleError));
  }

  public post<T>(url: string, body: object): Observable<T | HttpErrorModel> {
    const fullUrl = `${this.xapiUrl}${removeStartSlash(url)}`;
    return this.http.post<T>(fullUrl, body, { withCredentials: true }).pipe(
      catchError(this.handleError));
  }

  /**
   * Handles any http errors.
   * @param err: the error
   * @return an observable error containing json conforming to HttpErrorModel interface
   */
  /* istanbul ignore next */
  private handleError(err: HttpErrorResponse): Observable<HttpErrorModel> {
    // console.debug('HttpErrorResponse is:', err);
    let errMsg: string;
    let returnObject: HttpErrorModel;
    if (err.error instanceof Error) {
      // console.debug('Error was instance of Error, meaning client side or network error', err);
      errMsg = err.error.message ? err.error.message : err.error.toString();
      returnObject = {
        errMsg,
        body: err.error,
        statusCode: err.status,
        statusText: err.statusText,
      };
    } else {
      // console.debug('Error was an actual error from the backend');
      const body = err.error;
      const errText = JSON.stringify(body);
      errMsg = `${err.status} - ${err.statusText} ${errText}`;
      returnObject = {
        errMsg,
        body,
        statusCode: err.status,
        statusText: err.statusText,
      };
    }
    return throwError(returnObject);
  }
}

@NgModule()
export class HttpModule {

  constructor(@Optional() @SkipSelf() parentModule: HttpModule) {
    throwIfAlreadyLoaded(parentModule, 'HttpModule');
  }

  static forRoot(options: HttpModuleOptions): ModuleWithProviders {
    return {
      ngModule: HttpModule,
      providers: [
        options.httpOptionsProvider ||
        {
          provide: HTTP_MODULE_CONFIG,
          useValue: options.config,
        },
        HttpService,
      ],
    };
  }
}
