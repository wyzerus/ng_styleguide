import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';

/**
 * Script loader service, this uses the new way of registering services by using providedIn: 'root'. This means it does not need
 * to be registered as a provider in any modules.
 *
 * @export
 */
@Injectable({
  providedIn: 'root',
})
export class ScriptLoaderService {
  private scripts: ScriptModel[] = [];

  public load(script: ScriptModel): Observable<ScriptModel> {
    return new Observable<ScriptModel>((observer: Observer<ScriptModel>) => {
      const existingScript = this.scripts.find((existingScriptModel: ScriptModel) => existingScriptModel.name === script.name);

      // Complete if already loaded
      if (existingScript && existingScript.loaded) {
        observer.next(existingScript);
        observer.complete();
      } else if (!script.src) {
        observer.error('Could not load script as src property was empty');
      } else {
        // Add the script
        this.scripts = [...this.scripts, script];

        // Load the script
        const scriptElement = document.createElement('script');
        scriptElement.type = 'text/javascript';
        scriptElement.src = script.src;
        if (script.id) {
          scriptElement.id = script.id;
        }

        // Can only have async or defer, never both
        if (script.async) {
          scriptElement.async = true;
        } else if (script.defer) {
          scriptElement.defer = true;
        }

        scriptElement.onload = () => {
          script.loaded = true;
          observer.next(script);
          observer.complete();
        };

        scriptElement.onerror = (error: any) => {
          observer.error(`Couldn't load script ${script.src} - Error is: ${error}`);
        };

        document.getElementsByTagName('head')[0].appendChild(scriptElement);
      }
    });
  }
}

/**
 * Model describing the properties a script should have.
 *
 * @export
 */
export interface ScriptModel {
  /**
   * The name, should be a unique value, duplicate names wont get added.
   * @memberof ScriptModel
   */
  name: string;
  /**
   * The source URL for the javascript file, ideally should start with // so that
   * the protocol will be automatically added for http and https.
   * @memberof ScriptModel
   */
  src: string;
  loaded: boolean;
  id?: string;
  async?: boolean;
  defer?: boolean;
}
