import {
  ErrorHandler, Injector, NgModule, Optional, SkipSelf,
  ModuleWithProviders, InjectionToken, Inject, NgZone,
} from '@angular/core';
import { Router } from '@angular/router';

import { throwIfAlreadyLoaded } from '../../guards/module-import.guard';

export const ERROR_HANDLER_OPTIONS = new InjectionToken<ErrorHandlerModuleOptions>('ERROR_HANDLER_OPTIONS');

/**
 * Interface for the options this module takes when being imported into the app using forRoot() method.
 */
export interface ErrorHandlerModuleOptions {
  /**
   * The URL to redirect to when an error occurs, will default to '/error/500' if left empty.
   *
   * @memberof ErrorHandlerModuleOptions
   */
  errorUrl?: string;
}

/**
 * Custom error handler, the default Angular one just console logs an error,
 * this does the same but also will redirect to the 500 error page.
 */
export class ErrorHandlerService implements ErrorHandler {

  /**
   * The error page to redirect to.
   *
   * @memberof ErrorHandlerService
   */
  private errorPage: string;

  constructor(private injector: Injector, @Optional() @Inject(ERROR_HANDLER_OPTIONS) config: ErrorHandlerModuleOptions,
    private zone: NgZone) {
    this.errorPage = config && config.errorUrl ? config.errorUrl : '/error/500';
  }

  get router(): Router {
    return this.injector.get(Router);
  }

  handleError(error: any): void {
    // tslint:disable-next-line:no-console
    console.error(error);
    // As we are outside of Angular zone at this point, need to run the router navigate inside ngzone
    this.zone.run(() => this.router.navigateByUrl(this.errorPage));
  }
}

@NgModule()
export class ErrorHandlerModule {

  constructor(@Optional() @SkipSelf() parentModule: ErrorHandlerModule) {
    throwIfAlreadyLoaded(parentModule, 'ErrorHandlerModule');
  }

  static forRoot(options: ErrorHandlerModuleOptions): ModuleWithProviders {
    return {
      ngModule: ErrorHandlerModule,
      providers: [
        {
          provide: ERROR_HANDLER_OPTIONS,
          useValue: options,
        },
        {
          provide: ErrorHandler,
          useClass: ErrorHandlerService,
        },
      ],
    };
  }
}
