import { Injector, ErrorHandler } from '@angular/core';
import { TestBed } from '@angular/core/testing';

import { ErrorHandlerService, ErrorHandlerModule, ErrorHandlerModuleOptions } from './error-handler';

describe('ErrorHandler', () => {

  describe('ErrorHandlerModule passing null to forRoot', () => {
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [
          ErrorHandlerModule.forRoot(null),
        ],
      });
    });

    it('should provide services', () => {
      expect(TestBed.get(ErrorHandler)).toBeObject();
    });
  });

  describe('ErrorHandlerModule passing an object to forRoot', () => {
    const moduleOptions: ErrorHandlerModuleOptions = {
      errorUrl: '/customerrorUrl',
    };

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [
          ErrorHandlerModule.forRoot(moduleOptions),
        ],
      });
    });

    it('should redirect to the passed in errorUrl value', () => {
      const mockRouter = { navigateByUrl: () => { } };
      const errorHandler = TestBed.get(ErrorHandler);
      expect(errorHandler).toBeObject();
      expect(errorHandler instanceof ErrorHandlerService).toBeTrue();
      spyOn(mockRouter, 'navigateByUrl');
      const injector = TestBed.get(Injector);
      spyOn(injector, 'get').and.returnValue(mockRouter);
      errorHandler.handleError(null);
      expect(mockRouter.navigateByUrl).toHaveBeenCalledWith('/customerrorUrl');
    });
  });

  describe('ErrorHandlerModule loaded twice', () => {
    it('should throw an error if parent is not null', () => {
      // tslint:disable:no-unused-expression
      expect(() => { new ErrorHandlerModule(new ErrorHandlerModule(null)); }).toThrowError();
    });
  });

});
