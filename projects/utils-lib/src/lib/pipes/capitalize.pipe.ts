import { Pipe, PipeTransform, NgModule } from '@angular/core';

/**
 * Capitalizes the first letter of the passed in string.
 *
 * @export
 */
@Pipe({ name: 'esCapitalize' })
export class CapitalizePipe implements PipeTransform {

  transform(value: string): string {
    if (value) {
      return value.charAt(0).toUpperCase() + value.slice(1);
    }
    return value;
  }
}

/**
 * Capitalise pipe module.
 *
 * @export
 */
@NgModule({
  exports: [CapitalizePipe],
  declarations: [CapitalizePipe],
})
export class CapitalizePipeModule {
}
