import { PluralPipe } from './plural.pipe';

// SRC: https://www.grammarly.com/blog/plural-nouns/

describe('PluralPipe', () => {

  const pipe: PluralPipe = new PluralPipe();

  describe('regular nouns (goat -> goats)', () => {

    it('should return singular with no rule given', () => {
      expect(pipe.transform(1, 'goat')).toBe('1 goat');
    });

    it('should return singular with rule given', () => {
      expect(pipe.transform(1, 'goat', 's')).toBe('1 goat');
    });

    it('should return plural with no rule given and zero value', () => {
      expect(pipe.transform(0, 'goat')).toBe('0 goats');
    });

    it('should return plural with rule given and zero value', () => {
      expect(pipe.transform(0, 'goat', 's')).toBe('0 goats');
    });

    it('should return plural with no rule given and value greater than 1', () => {
      expect(pipe.transform(2, 'goat')).toBe('2 goats');
      expect(pipe.transform(99, 'goat')).toBe('99 goats');
    });

    it('should return plural with rule given and value greater than 1', () => {
      expect(pipe.transform(2, 'goat', 's')).toBe('2 goats');
      expect(pipe.transform(99, 'goat', 's')).toBe('99 goats');
    });

    it('should return plural with zero value replaced', () => {
      expect(pipe.transform(0, 'goat', 's', 'No')).toBe('No goats');
      expect(pipe.transform(0, 'goat', 's', 'Zero')).toBe('Zero goats');
    });

  });

  describe('singular nouns ending in ‑s, -ss, -sh, -ch, -x, or -z (lunch -> lunches)', () => {

    it('should return singular', () => {
      expect(pipe.transform(1, 'lunch', 'es')).toBe('1 lunch');
    });

    it('should return plural with zero value', () => {
      expect(pipe.transform(0, 'lunch', 'es')).toBe('0 lunches');
    });

    it('should return plural with value greater than 1', () => {
      expect(pipe.transform(2, 'lunch', 'es')).toBe('2 lunches');
      expect(pipe.transform(99, 'lunch', 'es')).toBe('99 lunches');
    });

    it('should return plural with zero value replaced', () => {
      expect(pipe.transform(0, 'lunch', 'es', 'No')).toBe('No lunches');
      expect(pipe.transform(0, 'lunch', 'es', 'Zero')).toBe('Zero lunches');
    });

  });

  describe('singular nouns ending in -s or -z, require that you double the -s or -z prior to adding the -es (lunch -> lunches)', () => {

    it('should return singular', () => {
      expect(pipe.transform(1, 'gas', 'ses')).toBe('1 gas');
    });

    it('should return plural with zero value', () => {
      expect(pipe.transform(0, 'gas', 'ses')).toBe('0 gasses');
    });

    it('should return plural with value greater than 1', () => {
      expect(pipe.transform(2, 'gas', 'ses')).toBe('2 gasses');
      expect(pipe.transform(99, 'gas', 'ses')).toBe('99 gasses');
    });

    it('should return plural with zero value replaced', () => {
      expect(pipe.transform(0, 'gas', 'ses', 'No')).toBe('No gasses');
      expect(pipe.transform(0, 'gas', 'ses', 'Zero')).toBe('Zero gasses');
    });

  });

  describe('singular noun ends with ‑f or ‑fe, changed to ‑ves (knife -> knives)', () => {

    it('should return singular', () => {
      expect(pipe.transform(1, 'knife', '--ves')).toBe('1 knife');
    });

    it('should return plural with zero value', () => {
      expect(pipe.transform(0, 'knife', '--ves')).toBe('0 knives');
    });

    it('should return plural with value greater than 1', () => {
      expect(pipe.transform(2, 'knife', '--ves')).toBe('2 knives');
      expect(pipe.transform(99, 'knife', '--ves')).toBe('99 knives');
    });

    it('should return plural with zero value replaced', () => {
      expect(pipe.transform(0, 'knife', '--ves', 'No')).toBe('No knives');
      expect(pipe.transform(0, 'knife', '--ves', 'Zero')).toBe('Zero knives');
    });

  });

  describe('singular noun ends in ‑y and the letter before the -y is a consonant, changes to ‑ies (puppy -> puppies)', () => {

    it('should return singular', () => {
      expect(pipe.transform(1, 'puppy', '-ies')).toBe('1 puppy');
    });

    it('should return plural with zero value', () => {
      expect(pipe.transform(0, 'puppy', '-ies')).toBe('0 puppies');
    });

    it('should return plural with value greater than 1', () => {
      expect(pipe.transform(2, 'puppy', '-ies')).toBe('2 puppies');
      expect(pipe.transform(99, 'puppy', '-ies')).toBe('99 puppies');
    });

    it('should return plural with zero value replaced', () => {
      expect(pipe.transform(0, 'puppy', '-ies', 'No')).toBe('No puppies');
      expect(pipe.transform(0, 'puppy', '-ies', 'Zero')).toBe('Zero puppies');
    });

  });

  describe('singular noun ends in ‑o, add ‑es  (potato -> potatoes)', () => {

    it('should return singular', () => {
      expect(pipe.transform(1, 'potato', 'es')).toBe('1 potato');
    });

    it('should return plural with zero value', () => {
      expect(pipe.transform(0, 'potato', 'es')).toBe('0 potatoes');
    });

    it('should return plural with value greater than 1', () => {
      expect(pipe.transform(2, 'potato', 'es')).toBe('2 potatoes');
      expect(pipe.transform(99, 'potato', 'es')).toBe('99 potatoes');
    });

    it('should return plural with zero value replaced', () => {
      expect(pipe.transform(0, 'potato', 'es', 'No')).toBe('No potatoes');
      expect(pipe.transform(0, 'potato', 'es', 'Zero')).toBe('Zero potatoes');
    });

  });

  describe('singular noun ends in ‑us, the plural ending is frequently ‑i (cactus -> cacti)', () => {

    it('should return singular', () => {
      expect(pipe.transform(1, 'cactus', '--i')).toBe('1 cactus');
    });

    it('should return plural with zero value', () => {
      expect(pipe.transform(0, 'cactus', '--i')).toBe('0 cacti');
    });

    it('should return plural with value greater than 1', () => {
      expect(pipe.transform(2, 'cactus', '--i')).toBe('2 cacti');
      expect(pipe.transform(99, 'cactus', '--i')).toBe('99 cacti');
    });

    it('should return plural with zero value replaced', () => {
      expect(pipe.transform(0, 'cactus', '--i', 'No')).toBe('No cacti');
      expect(pipe.transform(0, 'cactus', '--i', 'Zero')).toBe('Zero cacti');
    });

  });

  describe('singular noun ends in ‑is, the plural ending is ‑es (ellipsis -> ellipses)', () => {

    it('should return singular', () => {
      expect(pipe.transform(1, 'ellipsis', '--es')).toBe('1 ellipsis');
    });

    it('should return plural with zero value', () => {
      expect(pipe.transform(0, 'ellipsis', '--es')).toBe('0 ellipses');
    });

    it('should return plural with value greater than 1', () => {
      expect(pipe.transform(2, 'ellipsis', '--es')).toBe('2 ellipses');
      expect(pipe.transform(99, 'ellipsis', '--es')).toBe('99 ellipses');
    });

    it('should return plural with zero value replaced', () => {
      expect(pipe.transform(0, 'ellipsis', '--es', 'No')).toBe('No ellipses');
      expect(pipe.transform(0, 'ellipsis', '--es', 'Zero')).toBe('Zero ellipses');
    });

  });

  describe('singular noun ends in ‑on, the plural ending is ‑a (phenomenon -> phenomena)', () => {

    it('should return singular', () => {
      expect(pipe.transform(1, 'phenomenon', '--a')).toBe('1 phenomenon');
    });

    it('should return plural with zero value', () => {
      expect(pipe.transform(0, 'phenomenon', '--a')).toBe('0 phenomena');
    });

    it('should return plural with value greater than 1', () => {
      expect(pipe.transform(2, 'phenomenon', '--a')).toBe('2 phenomena');
      expect(pipe.transform(99, 'phenomenon', '--a')).toBe('99 phenomena');
    });

    it('should return plural with zero value replaced', () => {
      expect(pipe.transform(0, 'phenomenon', '--a', 'No')).toBe('No phenomena');
      expect(pipe.transform(0, 'phenomenon', '--a', 'Zero')).toBe('Zero phenomena');
    });

  });

  describe('Irregular nouns', () => {
    describe('Irregular noun (child -> children)', () => {

      it('should return singular', () => {
        expect(pipe.transform(1, 'child', 'ren')).toBe('1 child');
      });

      it('should return plural with phenomenon value', () => {
        expect(pipe.transform(0, 'child', 'ren')).toBe('0 children');
      });

      it('should return plural with value greater than 1', () => {
        expect(pipe.transform(2, 'child', 'ren')).toBe('2 children');
        expect(pipe.transform(99, 'child', 'ren')).toBe('99 children');
      });

      it('should return plural with zero value replaced', () => {
        expect(pipe.transform(0, 'child', 'ren', 'No')).toBe('No children');
        expect(pipe.transform(0, 'child', 'ren', 'Zero')).toBe('Zero children');
      });

    });

    describe('Irregular noun (mouse -> mice)', () => {

      it('should return singular', () => {
        expect(pipe.transform(1, 'mouse', '=mice')).toBe('1 mouse');
      });

      it('should return plural with phenomenon value', () => {
        expect(pipe.transform(0, 'mouse', '=mice')).toBe('0 mice');
      });

      it('should return plural with value greater than 1', () => {
        expect(pipe.transform(2, 'mouse', '=mice')).toBe('2 mice');
        expect(pipe.transform(99, 'mouse', '=mice')).toBe('99 mice');
      });

      it('should return plural with zero value replaced', () => {
        expect(pipe.transform(0, 'mouse', '=mice', 'No')).toBe('No mice');
        expect(pipe.transform(0, 'mouse', '=mice', 'Zero')).toBe('Zero mice');
      });

    });

    describe('Irregular noun (person -> mice)', () => {

      it('should return singular', () => {
        expect(pipe.transform(1, 'person', '=people')).toBe('1 person');
      });

      it('should return plural with phenomenon value', () => {
        expect(pipe.transform(0, 'person', '=people')).toBe('0 people');
      });

      it('should return plural with value greater than 1', () => {
        expect(pipe.transform(2, 'person', '=people')).toBe('2 people');
        expect(pipe.transform(99, 'person', '=people')).toBe('99 people');
      });

      it('should return plural with zero value replaced', () => {
        expect(pipe.transform(0, 'person', '=people', 'No')).toBe('No people');
        expect(pipe.transform(0, 'person', '=people', 'Zero')).toBe('Zero people');
      });

    });

  });
});
