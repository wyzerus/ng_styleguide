import { Pipe, PipeTransform, NgModule } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

/**
 * Bypasses Angular security by marking the URL as safe.
 *
 * @export
 */
@Pipe({ name: 'esSafeUrl' })
export class SafeUrlPipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) { }

  transform(url: string): SafeResourceUrl {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}

/**
 * Safe url pipe module.
 *
 * @export
 */
@NgModule({
  exports: [SafeUrlPipe],
  declarations: [SafeUrlPipe],
})
export class SafeUrlPipeModule {
}
