import { Pipe, PipeTransform, NgModule } from '@angular/core';

/**
 * Pluralizes the given noun, check the unit test file for examples of usage.
 *
 * @export
 */
@Pipe({ name: 'esPlural' })
export class PluralPipe implements PipeTransform {

  transform(value: number, noun: string, rule: string = 's', zeroValue?: string): string {

    if (value === 1) { return `1 ${noun}`; }

    let modifiedNoun: string;

    if (rule.charAt(0) === '=') {
      modifiedNoun = rule.substr(1);
    } else {
      const charsToDelete = rule.lastIndexOf('-') + 1;

      modifiedNoun = (charsToDelete > 0)
        ? `${noun.slice(0, -charsToDelete)}${rule.substr(charsToDelete)}`
        : `${noun}${rule}`;
    }

    if (value === 0 && zeroValue) {
      return `${zeroValue} ${modifiedNoun}`;
    }

    return `${value} ${modifiedNoun}`;
  }
}

/**
 * Plural pipe module.
 *
 * @export
 */
@NgModule({
  exports: [PluralPipe],
  declarations: [PluralPipe],
})
export class PluralPipeModule {
}
