import { NgModule, Pipe, PipeTransform } from '@angular/core';
import { DateTime, Settings } from 'luxon';
Settings.defaultZoneName = 'utc';

/**
 * Sets time and date format with st/nd/rd/th suffix
 *
 * @export
 */
const getOrdinalSuffix = (n: number) => {
  const suffixes = ['th', 'st', 'nd', 'rd'];
  const v = n % 100;
  return suffixes[(v - 20) % 10] || suffixes[v] || suffixes[0];
};

interface DateStringPipeOptions {
  format?: string;
  ordinal?: boolean;
}

@Pipe({name: 'esDateString'})
export class DateStringPipe implements PipeTransform {

  transform(date: string, options?: DateStringPipeOptions | string): string {
    let format: string;
    let ordinal: boolean;

    if (typeof options === 'object') {
      ({ format, ordinal } = options);
    } else {
      format = options;
    }

    if (!format) {
      format = 'fullDateTime';
    }

    let output: string;

    const dateTime = DateTime.fromISO(date);
    const dateFormat = `d${(ordinal) ? '!' : ''} LLL yyyy`;

    switch (format) {
      case 'fullDateTime':
        output = `${dateTime.toFormat(dateFormat)} at ${dateTime.toFormat('HH:mm a').toLowerCase()}`;
        break;
      case 'fullDate':
        output = dateTime.toFormat(dateFormat);
        break;
      default:
        output = dateTime.toFormat((ordinal) ? format.replace('d ', 'd! ') : format);
    }
    if (ordinal) {
      const suffix = getOrdinalSuffix(dateTime.day);
      output = output.replace('!', suffix);
    }
    return output;
  }

}

/**
 * DateString pipe module.
 *
 * @export
 */
@NgModule({
  exports: [DateStringPipe],
  declarations: [DateStringPipe],
})

export class DateStringPipeModule { }
