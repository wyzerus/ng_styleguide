import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { inject, TestBed } from '@angular/core/testing';

import { SafeUrlPipe, SafeUrlPipeModule } from './safe-url.pipe';

describe('SafeUrlPipe', () => {
  beforeEach(() => {
    TestBed
      .configureTestingModule({
        imports: [
          SafeUrlPipeModule,
        ],
      });
  });

  it('should create an instance and allow the url', inject([DomSanitizer], (domSanitizer: DomSanitizer) => {
    const pipe = new SafeUrlPipe(domSanitizer);
    expect(pipe).toBeObject();
    const returnVal: SafeResourceUrl = pipe.transform('https://google.co.uk');
    expect(returnVal).toBeObject();
  }));
});
