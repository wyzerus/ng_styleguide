import { DateStringPipe } from './date-string.pipe';
import { Settings } from 'luxon';
Settings.defaultZoneName = 'utc';

const mockStartDate = {
  startDate: '2018-10-21T01:02:03Z',
};

describe('DateStringPipe', () => {
  let pipe: DateStringPipe;

  beforeEach(() => {
    pipe = new DateStringPipe();
  });

  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('provided with no format defaults to fullDateTime format of: 21 Oct 2018 at 01:02 am', () => {
    expect(pipe.transform(mockStartDate.startDate)).toBe('21 Oct 2018 at 01:02 am');
  });

  it('provided with date format fullDate formats to the date to: 21 Oct 2018', () => {
    expect(pipe.transform(mockStartDate.startDate, 'fullDate')).toBe('21 Oct 2018');
  });

  it('should take luxon strings as a format', () => {
    expect(pipe.transform(mockStartDate.startDate, 'yyyy LLL dd')).toBe('2018 Oct 21');
  });
});
