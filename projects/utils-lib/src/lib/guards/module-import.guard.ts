/**
 * Throws an error if the module has already been loaded.
 *
 * @export
 * @param parentModule the parent module
 * @param moduleName class name of the module
 */
export function throwIfAlreadyLoaded(parentModule: any, moduleName: string) {
  if (parentModule) {
    throw Error(`${moduleName} has already been loaded. It is designed to only be imported once.`);
  }
}
