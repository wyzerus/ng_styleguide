import { AfterViewInit, Directive, ElementRef, OnDestroy, NgModule } from '@angular/core';
import { IFrameComponent, iframeResizer } from 'iframe-resizer';

/**
 * Directive to use on an iframe when wanting automatic height resizing to the height of the elements in the iframe.
 * This depends on some Javascript also existing in the source code of the iframe itself.
 * https://github.com/davidjbradshaw/iframe-resizer
 *
 * @export
 */
@Directive({
  selector: '[esIframeResizer]',
})
export class IFrameResizerDirective implements AfterViewInit, OnDestroy {
  component: IFrameComponent;

  constructor(public element: ElementRef) {
  }

  ngAfterViewInit() {
    const components = iframeResizer(
      {
        /* This must be false because the iframe resizer js file that gets included in the datacash template
        is hosted on different domain to the datacash iframe */
        checkOrigin: false,
        // heightCalculationMethod: 'documentElementOffset',
        log: false,
      },
      this.element.nativeElement);

    /* save component reference so we can close it later */
    this.component = components && components.length > 0 ? components[0] : null;
  }

  ngOnDestroy(): void {
    if (this.component && this.component.iFrameResizer) {
      this.component.iFrameResizer.close();
    }
  }
}

/**
 * IFrameResizerDirective module.
 *
 * @export
 */
@NgModule({
  exports: [IFrameResizerDirective],
  declarations: [IFrameResizerDirective],
})
export class IFrameResizerDirectiveModule {
}
