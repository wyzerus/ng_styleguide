import { Component, Input, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

/**
 * Global messages component for displaying global messages. Takes in an array of messages.
 *
 * @export
 */
@Component({
  selector: 'es-global-messages',
  templateUrl: './global-messages.html',
})
export class GlobalMessagesComponent {

  /**
   * The array of messages to show.
   * @memberof GlobalMessagesComponent
   */
  @Input() messages: Message[];

  constructor() { }

  /**
   * Determines if there are any messages to show by checking the array has some messages in it.
   *
   * @returns true if there are some messages, false otherwise
   * @memberof GlobalMessagesComponent
   */
  public hasMessages(): boolean {
    return this.messages && this.messages.length > 0;
  }

  /**
   * Closes a message, essentially by removing it from the array of messages.
   *
   * @param index the index position of the message to close
   * @memberof GlobalMessagesComponent
   */
  public closeMessage(index: number): void {
    this.messages.splice(index, 1);
  }

}

/**
 * Interface to describe the properties a notification message should have.
 */
export interface Message {

  /** Severity, these match the class names of Bulma notifications, without the is- at the beginning. */
  severity: 'danger' | 'warning' | 'info' | 'success' | 'link' | 'primary';

  /** Heading of the message, this is optional. */
  heading?: string;

  /** The text of the message, this is optional. Can contain standard HTML code but not any Angular code. */
  summary?: string;

  /** Whether the message is closable. */
  closable: boolean;
}

/**
 * Global messages module providing anything to do with global messages.
 *
 * @export
 */
@NgModule({
  imports: [CommonModule],
  exports: [GlobalMessagesComponent],
  declarations: [GlobalMessagesComponent],
})
export class GlobalMessagesModule {
}
