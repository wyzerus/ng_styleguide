import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Component } from '@angular/core';
import { By } from '@angular/platform-browser';

import { BackToTopModule } from './back-to-top';

@Component({
  template: '<es-back-to-top id="noParams"></es-back-to-top>',
})
class TestBackTopComponent {
}

describe('BackToTopComponent', () => {

  let fixture: ComponentFixture<TestBackTopComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        TestBackTopComponent,
      ],
      providers: [],
      imports: [
        BackToTopModule,
        NoopAnimationsModule,
      ],
    });

    fixture = TestBed.createComponent(TestBackTopComponent);
    fixture.detectChanges();
  });

  it('should add the static classes and attributes', () => {
    const element: HTMLElement = fixture.debugElement.query(By.css('#noParams')).nativeElement;
    expect(element.getAttribute('aria-label')).toBeNonEmptyString();
    expect(element.getAttribute('aria-disabled')).toBe('false');
    expect(element.getAttribute('tabindex')).toBe('0');
    expect(element.getAttribute('role')).toBe('button');
    expect(element.classList).toContain('back-to-top');
  });

  it('should handle a click event', fakeAsync(() => {
    const element: HTMLElement = fixture.debugElement.query(By.css('#noParams')).nativeElement;
    element.click();
    tick(1000);
  }));
});

