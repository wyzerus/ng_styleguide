import {
  ChangeDetectionStrategy, Component, NgModule, HostListener, Input, Output, EventEmitter, ChangeDetectorRef,
  OnDestroy, OnInit, HostBinding,
} from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { interval, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

/**
 * Back to top component.
 *
 * @export
 */
@Component({
  selector: 'es-back-to-top',
  templateUrl: './back-to-top.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [trigger('buttonFadeInOut', [
    state('show', style({
      opacity: 0.8,
      bottom: '*',
    })),
    state('hide', style({
      opacity: 0,
      bottom: '-1.2rem',
    })),
    transition('show => hide', animate('500ms 100ms cubic-bezier(.25,.8,.25,1)')),
    transition('hide => show', animate('500ms cubic-bezier(.25,.8,.25,1)')),
  ])],
})
export class BackToTopComponent implements OnDestroy, OnInit {

  /**
   * Animated scrolling speed.
   * @default {80}
   */
  @Input() speed: number;

  /**
   * Acceleration coefficient, added to speed when using animated scroll.
   * @default {0}
   */
  @Input() acceleration: number;

  /**
   * If true scrolling to top will be animated.
   * @default {true}
   */
  @Input() animate: boolean;

  /**
   * If true no scrolling to top will happen when clicking on the button.
   *
   * @memberof BackToTopComponent
   */
  @Input() disabled: boolean;

  /**
   * The BackToTop button would appear on the page when scrollY is on this distance.
   * If it is set to 0, the button would always appear on the page.
   *
   * @memberof BackToTopComponent
   */
  @Input() showOnDistance: number;

  /**
   * Used to set the 'aria-label' attribute for a meaningful label.
   *
   * @memberof BackToTopComponent
   */
  @Input() ariaLabel: string;

  /**
   * EventEmitter when scrolling to the top.
   *
   * @memberof BackToTopComponent
   */
  @Output() reachTop: EventEmitter<boolean>;

  /**
   * Angular change detector ref, used as this components manages it's own change detection.
   *
   * @memberof BackToTopComponent
   */
  private cdRef: ChangeDetectorRef;

  /**
   * This subject is used to complete the interval observable that is created when animating back to the top.
   *
   * @memberof BackToTopComponent
   */
  private destroySubject: Subject<boolean> = new Subject<boolean>();

  /**
   * Flag to prevent multiple animations trying to run if user clicks multile times on the button whilst it is still animating.
   *
   * @memberof BackToTopComponent
   */
  private onScrolling: boolean;

  /**
   * State of the button, hidden or shown, used by the animations.
   *
   * @memberof BackToTopComponent
   */
  private buttonState: 'hide' | 'show';

  /**
   * Creates an instance of BackToTopComponent.
   * @param cdRef Angular change detector ref
   * @memberof BackToTopComponent
   */
  constructor(cdRef: ChangeDetectorRef) {
    this.acceleration = 0;
    this.animate = true;
    this.buttonState = 'hide';
    this.disabled = false;
    this.onScrolling = false;
    this.showOnDistance = 200;
    this.speed = 80;
    this.reachTop = new EventEmitter();
    this.cdRef = cdRef;
    this.ariaLabel = 'Scroll Back to Top';
  }

  /**
   * Add the class back-to-top to the host element.
   *
   * @memberof BackToTopComponent
   */
  @HostBinding('class.back-to-top')
  hostClasses: boolean = true;

  /**
   * Add the aria-label attribute to the host element.
   *
   * @readonly
   * @memberof BackToTopComponent
   */
  @HostBinding('attr.aria-label')
  get backTopAriaLabel(): string {
    return this.ariaLabel;
  }

  /**
   * Add the aria-disabled attribute to the host element.
   *
   * @readonly
   * @memberof BackToTopComponent
   */
  @HostBinding('attr.aria-disabled')
  get backTopAriaDisabled(): boolean {
    return this.disabled;
  }

  /**
   * Add the tabindex to the host element.
   *
   * @readonly
   * @memberof BackToTopComponent
   */
  @HostBinding('attr.tabindex')
  get backTopTabindex(): number {
    return this.disabled ? -1 : 0;
  }

  /**
   * Add the attrbute role of button to the host element.
   *
   * @memberof BackToTopComponent
   */
  @HostBinding('attr.role')
  role: string = 'button';

  /**
   * Run when the component is created, checks the inputs passed in are valid.
   *
   * @memberof BackToTopComponent
   */
  ngOnInit(): void {
    this.onScrolling = false;
    this.validateInputs();
    this.checkScrollDistance();
  }

  /**
   * Emit true from the destroySubject which will cause any open observables to complete.
   *
   * @memberof BackToTopComponent
   */
  ngOnDestroy(): void {
    this.destroySubject.next(true);
    this.destroySubject.unsubscribe();
  }

  /**
   * Sets the animation on the host element.
   *
   * @readonly
   * @memberof BackToTopComponent
   */
  @HostBinding('@buttonFadeInOut')
  get buttonFadeInOutAnimation() {
    return this.buttonState;
  }

  /**
   * Listens to the click event on the host element.
   *
   * @memberof BackToTopComponent
   */
  @HostListener('click')
  public handleClickOnHost(): void {
    if (this.onScrolling || this.disabled) {
      return;
    }
    this.scrollToTop();
    return;
  }

  /**
   * Listens to window scroll and animates the button.
   */
  @HostListener('window:scroll')
  public onWindowScroll(): void {
    if (this.showOnDistance === 0) {
      return;
    }
    this.updateScrollButtonState(window.scrollY);
  }

  /**
   * Update the button state.
   *
   * @param distance
   * @memberof BackToTopComponent
   */
  private updateScrollButtonState(distance: number): void {
    const prevState = this.buttonState;
    this.buttonState = distance > this.showOnDistance ? 'show' : 'hide';
    if (this.buttonState !== prevState) {
      this.cdRef.markForCheck();
    }
  }

  /**
   * Get the current position of the scroll.
   *
   * @returns current scroll top position
   * @memberof BackToTopComponent
   */
  private getCurrentScrollTop(): number {
    if (typeof window.scrollY !== 'undefined') {
      return window.scrollY;
    }
    if (typeof window.pageYOffset !== 'undefined') {
      return window.pageYOffset;
    }
    if (typeof document.body.scrollTop !== 'undefined') {
      return document.body.scrollTop;
    }
    if (typeof document.documentElement.scrollTop !== 'undefined') {
      return document.documentElement.scrollTop;
    }
    return 0;
  }

  /**
   * Scroll to top.
   *
   * @memberof BackToTopComponent
   */
  private scrollToTop(): void {
    if (this.animate) {
      this.animateScrollTop();
    } else {
      window.scrollTo(0, 0);
      this.reachTop.emit(true);
    }
  }

  /**
   * Animate the scroll to top.
   *
   * @memberof BackToTopComponent
   */
  private animateScrollTop(): void {
    this.destroySubject.next(false);
    let initialSpeed = this.speed;
    this.onScrolling = true;

    interval(15).pipe(takeUntil(this.destroySubject)).subscribe(() => {
      window.scrollBy(0, -initialSpeed);
      initialSpeed = initialSpeed + this.acceleration;
      if (this.getCurrentScrollTop() === 0) {
        this.onScrolling = false;
        this.reachTop.emit(true);
        // Emit true on the subject to complete the observable
        this.destroySubject.next(true);
      }
    },
      ((error: any) => {
        // tslint:disable-next-line:no-console
        console.error('Error animating scroll to top', error);
      }));
  }

  /**
   * Validate the inputs passed into the component.
   *
   * @memberof BackToTopComponent
   */
  private validateInputs(): void {
    const errorMessagePrefix = 'BackToTopButton component input validation error: ';
    if (this.showOnDistance < 0) {
      throw Error(errorMessagePrefix + '\'showOnDistance\' parameter must be greater or equal to 0');
    }
    if (this.speed < 1) {
      throw Error(errorMessagePrefix + '\'speed\' parameter must be a positive number');
    }
    if (this.acceleration < 0) {
      throw Error(errorMessagePrefix + '\'acceleration\' parameter must be greater or equal to 0');
    }
  }

  /**
   * Check the scroll distance.
   *
   * @memberof BackToTopComponent
   */
  private checkScrollDistance(): void {
    if (this.showOnDistance === 0) {
      this.buttonState = 'show';
    }
    return;
  }

}

/**
 * Back to top module providing anything to do with back to top functionality.
 *
 * @export
 */
@NgModule({
  exports: [BackToTopComponent],
  declarations: [BackToTopComponent],
})
export class BackToTopModule {
}
