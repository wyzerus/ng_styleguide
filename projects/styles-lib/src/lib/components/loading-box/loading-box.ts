import { Component, NgModule, Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'es-loading-box',
  templateUrl: './loading-box.html',
})
export class LoadingBoxComponent {

  constructor() { }

}

/**
 * @dynamic
 *
 * Loading box service.
 * @export
 */
@Injectable({
  providedIn: 'root',
})
export class LoadingBoxService {

  /** Name of the css class to apply to indicate the nav has ended and so loading indicator should be hidden. */
  private readonly navEndedClass = 'navEnded';

  constructor(@Inject(DOCUMENT) private doc: Document) { }

  public showLoader() {
    const load: HTMLElement = this.getLoadingIndicator();
    if (load) {
      const classes = load.classList;
      // Remove the navEnded class
      load.classList.remove(this.navEndedClass);
      // Delay the showing of it, if the navEnded class has been added within the timeout value,
      // then we don't need to show the loading indicator
      setTimeout(
        () => {
          if (!classes.contains(this.navEndedClass)) {
            load.style.display = 'block';
          }
        },
        250);
    }
  }

  public hideLoader() {
    const load: HTMLElement = this.getLoadingIndicator();
    if (load) {
      load.classList.add(this.navEndedClass);
      load.style.display = 'none';
    }
  }

  private getLoadingIndicator(): HTMLElement {
    return this.doc.getElementById('loading-indicator');
  }

}

/**
 * Loading box module.
 *
 * @export
 */
@NgModule({
  declarations: [LoadingBoxComponent],
  exports: [LoadingBoxComponent],
})
export class LoadingBoxModule {
}
