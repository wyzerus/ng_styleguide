import { Component, Input, NgModule, HostBinding } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'es-help-text',
  templateUrl: './help-text.html',
})
export class HelpTextComponent {

  public showHide: boolean;

  /**
   * The help icon, defaults to false.
   */
  @Input() helpIcon?: boolean = false;

  @HostBinding('class.column')
  @HostBinding('class.is-12') hostClasses: boolean = true;

  /**
   * Shows or hides the help text by reversing it's current showHide status.
   *
   * @memberof HelpTextComponent
   */
  public toggleHelpText(): void {
    this.showHide = !this.showHide;
  }
}

/**
 * Help text module providing anything to do with help text.
 *
 * @export
 */
@NgModule({
  imports: [CommonModule],
  exports: [HelpTextComponent],
  declarations: [HelpTextComponent],
})
export class HelpTextModule {
}
