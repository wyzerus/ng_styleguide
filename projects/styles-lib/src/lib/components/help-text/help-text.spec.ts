import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpTextComponent, HelpTextModule } from './help-text';

describe('HelpTextComponent', () => {
  let component: HelpTextComponent;
  let fixture: ComponentFixture<HelpTextComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
      ],
      providers: [],
      imports: [HelpTextModule],
    });
    fixture = TestBed.createComponent(HelpTextComponent);
    component = fixture.componentInstance;
  });

  it('should create the component', () => {
    expect(component).toBeObject();
    expect(component instanceof HelpTextComponent).toBeTrue();
  });

  it('toggleHelpText should toggle the showHide status', () => {
    component.showHide = false;
    component.toggleHelpText();
    expect(component.showHide).toBeTrue();
  });
});
