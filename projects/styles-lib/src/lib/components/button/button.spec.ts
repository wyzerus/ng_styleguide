import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component } from '@angular/core';
import { By } from '@angular/platform-browser';

import { ButtonModule } from './button';

@Component({
  template: `<button esButton="action" id="actionButton">Action button</button>
  <button esButton="primary" id="primaryButton">Primary button</button>
  <button esButton="inverted" id="invertedButton">Inverted button</button>
  <button esButton="text" id="textButton">Text button</button>
  <button esButton="danger" esFullwidth="true" id="dangerButton">Danger button</button>
  <button esButton="warning" esSmall="true" id="warningButton">Warning button</button>
  <button esButton="info" esLarge="true" id="infoButton">Info button</button>
  <button esButton="outlined" id="outlinedButton">Outlined button</button>
  <button esButton="info" [esDisabled]="disabled" id="disabledButton">Disabled button</button>
  <button esButton="info" [esTooltip]="{ enabled: showTooltip, classes: ['is-tooltip-bottom-mobile', 'is-tooltip-left' ] }"
  data-tooltip="Tooltip text" id="tooltipButton">Tooltip button</button>`,
})
class TestButtonComponent {
  public disabled: boolean = true;
  public showTooltip: boolean = true;
}

describe('Button', () => {

  let component: TestButtonComponent;
  let fixture: ComponentFixture<TestButtonComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        TestButtonComponent,
      ],
      providers: [],
      imports: [
        ButtonModule,
      ],
    });

    fixture = TestBed.createComponent(TestButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should add the classes of button and is-rounded', () => {
    const button: HTMLButtonElement = fixture.debugElement.query(By.css('#actionButton')).nativeElement;
    expect(button.classList).toContain('button');
    expect(button.classList).toContain('is-rounded');
  });

  describe('Button type of action', () => {
    it('should add the class is-success', () => {
      const button: HTMLButtonElement = fixture.debugElement.query(By.css('#actionButton')).nativeElement;
      expect(button.classList).toContain('is-success');
    });
  });

  describe('Button type of primary', () => {
    it('should add the class is-link', () => {
      const button: HTMLButtonElement = fixture.debugElement.query(By.css('#primaryButton')).nativeElement;
      expect(button.classList).toContain('is-link');
    });
  });

  describe('Button type of inverted', () => {
    it('should add the class is-link and is-outlined', () => {
      const button: HTMLButtonElement = fixture.debugElement.query(By.css('#invertedButton')).nativeElement;
      expect(button.classList).toContain('is-link');
      expect(button.classList).toContain('is-outlined');
    });
  });

  describe('Button type of text', () => {
    it('should add the class is-text', () => {
      const button: HTMLButtonElement = fixture.debugElement.query(By.css('#textButton')).nativeElement;
      expect(button.classList).toContain('is-text');
    });
  });

  describe('Button type of danger', () => {
    it('should add the class is-danger and is-fullwidth', () => {
      const button: HTMLButtonElement = fixture.debugElement.query(By.css('#dangerButton')).nativeElement;
      expect(button.classList).toContain('is-danger');
      expect(button.classList).toContain('is-fullwidth');
    });
  });

  describe('Button type of warning', () => {
    it('should add the class is-warning and is-small', () => {
      const button: HTMLButtonElement = fixture.debugElement.query(By.css('#warningButton')).nativeElement;
      expect(button.classList).toContain('is-warning');
      expect(button.classList).toContain('is-small');
    });
  });

  describe('Button type of info', () => {
    it('should add the class is-info and is-large', () => {
      const button: HTMLButtonElement = fixture.debugElement.query(By.css('#infoButton')).nativeElement;
      expect(button.classList).toContain('is-info');
      expect(button.classList).toContain('is-large');
    });
  });

  describe('Button type of outlined', () => {
    it('should add the class is-outlined', () => {
      const button: HTMLButtonElement = fixture.debugElement.query(By.css('#outlinedButton')).nativeElement;
      expect(button.classList).toContain('is-outlined');
    });
  });

  describe('Button type of disabled', () => {
    it('should add and remove the class es-is-disabled depending on esDisabled value', () => {
      component.disabled = false;
      fixture.detectChanges();
      let button: HTMLButtonElement = fixture.debugElement.query(By.css('#disabledButton')).nativeElement;
      expect(button.classList).not.toContain('es-is-disabled');

      component.disabled = true;
      fixture.detectChanges();
      button = fixture.debugElement.query(By.css('#disabledButton')).nativeElement;
      expect(button.classList).toContain('es-is-disabled');
    });
  });

  describe('Button with a tooltip', () => {
    it('should add and remove tooltip classes depending on state of tooltip', () => {
      component.showTooltip = false;
      fixture.detectChanges();
      let button: HTMLButtonElement = fixture.debugElement.query(By.css('#tooltipButton')).nativeElement;
      expect(button.classList).toContain('is-tooltip-bottom-mobile');
      expect(button.classList).toContain('is-tooltip-left');
      expect(button.classList).not.toContain('tooltip');

      component.showTooltip = true;
      fixture.detectChanges();
      button = fixture.debugElement.query(By.css('#tooltipButton')).nativeElement;
      expect(button.classList).toContain('is-tooltip-bottom-mobile');
      expect(button.classList).toContain('is-tooltip-left');
      expect(button.classList).toContain('tooltip');
    });
  });

});
