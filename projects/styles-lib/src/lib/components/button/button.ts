import { Directive, HostBinding, NgModule, OnInit, Input, OnChanges } from '@angular/core';

/**
 * The possible button types that the directive can work with.
 * @export
 */
export type ButtonType = 'primary' | 'inverted' | 'action' | 'text' | 'danger' | 'warning' | 'info' | 'outlined';

/**
 * Tooltip information.
 *
 * @export
 */
export interface TooltipInfo {
  enabled: boolean;
  classes: string[];
}

/**
 * Directive for adding various button related classes to an element. Typically this can be used for adding to the following HTML elements:
 * <pre><code>
 * <button>
 * <a>
 * <input type="button">
 * <input type="submit">
 * </code></pre>
 * @export
 */
@Directive({
  selector: '[esButton]',
})
export class ButtonDirective implements OnInit, OnChanges {

  /**
   * The type of button.
   *
   * @memberof ButtonDirective
   */
  // tslint:disable-next-line:no-input-rename
  @Input('esButton') buttonType: ButtonType;

  /**
   * Whether the button is full width.
   *
   * @memberof ButtonDirective
   */
  @Input() esFullwidth: boolean = false;

  /**
   * Whether the button is to appear as disabled.
   *
   * @memberof ButtonDirective
   */
  @Input() esDisabled: boolean = false;

  @Input() esSmall: boolean = false;

  @Input() esLarge: boolean = false;

  /**
   * Tooltip information.
   * TODO: Perhaps tooltip could be it's own directive, so you would apply both esButton and esTooltip to the
   * button? This is what primeng do.
   *
   * @memberof ButtonDirective
   */
  @Input() esTooltip: TooltipInfo;

  /**
   * The CSS classes that will be applied to the host element this directive is on.
   *
   * @protected
   * @memberof ButtonDirective
   */
  protected _elementClass: string[] = ['button', 'is-rounded'];

  ngOnInit(): void {
    if (this.esTooltip) {
      // Always add the other classes, the class of tooltip (added/removed in ngOnChanges hook) is what determines if it is shown or not
      this._elementClass.push(...this.esTooltip.classes);
    }

    if (this.esFullwidth) {
      this._elementClass.push('is-fullwidth');
    }

    if (this.esSmall) {
      this._elementClass.push('is-small');
    }
    if (this.esLarge) {
      this._elementClass.push('is-large');
    }

    switch (this.buttonType) {
      case 'primary':
        this._elementClass.push('is-link');
        break;
      case 'inverted':
        this._elementClass.push('is-outlined', 'is-link');
        break;
      case 'action':
        this._elementClass.push('is-success');
        break;
      case 'text':
        this._elementClass.push('is-text');
        break;
      case 'danger':
        this._elementClass.push('is-danger');
        break;
      case 'warning':
        this._elementClass.push('is-warning');
        break;
      case 'info':
        this._elementClass.push('is-info');
        break;
      case 'outlined':
        this._elementClass.push('is-outlined');
        break;
      default:
        throw new Error('Unknown type passed into button component: ' + this.buttonType);
    }
  }

  /**
   * Adds or removes the tooltip class whenever the esTooltip property changes, and also the disabled class.
   *
   * @memberof ButtonDirective
   */
  ngOnChanges(): void {
    if (this.esTooltip) {
      if (this.esTooltip.enabled) {
        this._elementClass.push('tooltip');
      } else {
        const indexTooltip: number = this._elementClass.indexOf('tooltip');
        if (indexTooltip !== -1) {
          this._elementClass.splice(indexTooltip, 1);
        }
      }
    }

    if (this.esDisabled) {
      this._elementClass.push('es-is-disabled');
    } else {
      const indexDisabled: number = this._elementClass.indexOf('es-is-disabled');
      if (indexDisabled !== -1) {
        this._elementClass.splice(indexDisabled, 1);
      }
    }
  }

  /**
   * Gets the elementClass array and joins them into a single string.
   *
   * @readonly
   * @memberof ButtonDirective
   */
  @HostBinding('class')
  get elementClass(): string {
    return this._elementClass.join(' ');
  }

}

/**
 * Button module providing anything to do with buttons.
 *
 * @export
 */
@NgModule({
  exports: [ButtonDirective],
  declarations: [ButtonDirective],
})
export class ButtonModule {
}
