/*
 * Public API Surface of forms-lib
 */
export * from './lib/components/autocomplete/autocomplete';
export * from './lib/components/date/date';
export * from './lib/components/email/email';
export * from './lib/components/firstname/firstname';
export * from './lib/components/lastname/lastname';
export * from './lib/components/number/number';
export * from './lib/components/password/password';
export * from './lib/components/radio/radio';
export * from './lib/components/select/select';
export * from './lib/components/vehicle-reg/vehicle-reg';
export * from './lib/components/postcode/postcode';
