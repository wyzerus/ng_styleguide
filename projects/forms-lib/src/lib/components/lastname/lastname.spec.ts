import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LastnameModule, LastnameComponent } from './lastname';

describe('LastnameComponent', () => {

  let fixture: ComponentFixture<LastnameComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
      ],
      providers: [],
      imports: [
        LastnameModule,
      ],
    });
    fixture = TestBed.createComponent(LastnameComponent);
  });

  it('should throw an error if not all required attributes are passed in', () => {
    expect(() => fixture.detectChanges()).toThrowError();
  });

});
