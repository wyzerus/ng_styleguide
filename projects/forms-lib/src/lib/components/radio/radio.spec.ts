import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RadioComponent, RadioModule } from './radio';

describe('RadioComponent', () => {

  let fixture: ComponentFixture<RadioComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
      ],
      providers: [],
      imports: [
        RadioModule,
      ],
    });
    fixture = TestBed.createComponent(RadioComponent);
  });

  it('should throw an error if not all required attributes are passed in', () => {
    expect(() => fixture.detectChanges()).toThrowError();
  });

});
