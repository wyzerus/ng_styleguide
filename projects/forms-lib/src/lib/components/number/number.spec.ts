import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NumberModule, NumberComponent } from './number';

describe('NumberComponent', () => {

  let fixture: ComponentFixture<NumberComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
      ],
      providers: [],
      imports: [
        NumberModule,
      ],
    });
    fixture = TestBed.createComponent(NumberComponent);
  });

  it('should throw an error if not all required attributes are passed in', () => {
    expect(() => fixture.detectChanges()).toThrowError();
  });

});
