import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PasswordModule, PasswordComponent } from './password';

describe('PasswordComponent', () => {

  let fixture: ComponentFixture<PasswordComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
      ],
      providers: [],
      imports: [
        PasswordModule,
      ],
    });
    fixture = TestBed.createComponent(PasswordComponent);
  });

  it('should throw an error if not all required attributes are passed in', () => {
    expect(() => fixture.detectChanges()).toThrowError();
  });

});
