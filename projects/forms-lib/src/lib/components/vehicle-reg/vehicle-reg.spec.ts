import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleRegModule, VehicleRegComponent } from './vehicle-reg';

describe('VehicleRegComponent', () => {

  let fixture: ComponentFixture<VehicleRegComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
      ],
      providers: [],
      imports: [
        VehicleRegModule,
      ],
    });
    fixture = TestBed.createComponent(VehicleRegComponent);
  });

  it('should throw an error if not all required attributes are passed in', () => {
    expect(() => fixture.detectChanges()).toThrowError();
  });

});
