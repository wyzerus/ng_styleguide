import { Component, OnInit, Input, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AbstractControl, ReactiveFormsModule } from '@angular/forms';

import { FormFieldComponent } from '../form-field.component';

/**
 * Custom vehicle registration form field component.
 */
@Component({
  selector: 'es-vehicle-reg-field',
  templateUrl: './vehicle-reg.html',
})
export class VehicleRegComponent extends FormFieldComponent implements OnInit {

  /**
   * The field label, defaults to just Vehicle registration, so does not have to be passed in.
   */
  @Input() label?: string = 'Vehicle registration';

  /**
   * The value to use for the autocomplete attribute, has a default so does not have to be passed in.
   */
  @Input() autocomplete?: string = 'on';

  /**
   * The field is optional, pass in any required static string.
   */
  @Input() leftText?: string;

  /**
   * The field is optional, pass in any required static string.
   */
  @Input() rightText?: string;

  /**
   * Additional text to display underneath the label.
   */
  @Input() helpText?: string;

  /**
   * Message to display if field has a required validation error, has a default so does not need to be passed in.
   */
  @Input() requiredMessage?: string =
    'We do need this information to progress. Please check you have entered the details correctly for us to continue';

  /**
   * Constructor, does nothing.
   */
  constructor() {
    super();
  }

  ngOnInit() {
    if (this.parentFormGroup === undefined || this.submitted === undefined || this.controlName === undefined) {
      throw new Error('Some required attributes were not passed in to the es-vehicle-reg-field component');
    }
  }

  /**
   * Gets the FormControl for the number field this component represents.
   */
  get vehicleRegControl(): AbstractControl {
    return this.parentFormGroup.get(this.controlName);
  }

}

/**
 * Vehicle registration module providing anything to do with first vehicle registration form fields.
 *
 * @export
 */
@NgModule({
  imports: [CommonModule, ReactiveFormsModule],
  exports: [VehicleRegComponent],
  declarations: [VehicleRegComponent],
})
export class VehicleRegModule {
}
