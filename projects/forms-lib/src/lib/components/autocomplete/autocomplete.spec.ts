import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutocompleteComponent, AutocompleteModule } from './autocomplete';

describe('AutocompleteComponent', () => {

  let fixture: ComponentFixture<AutocompleteComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
      ],
      providers: [],
      imports: [
        AutocompleteModule,
      ],
    });
    fixture = TestBed.createComponent(AutocompleteComponent);
  });

  it('should throw an error if not all required attributes are passed in', () => {
    expect(() => fixture.detectChanges()).toThrowError();
  });

});
