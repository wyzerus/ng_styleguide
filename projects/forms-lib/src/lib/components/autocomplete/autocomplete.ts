import { Component, Input, OnInit, NgModule } from '@angular/core';
import { AbstractControl, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CompleterData, Ng2CompleterModule } from 'ng2-completer';

import { FormFieldComponent } from '../form-field.component';

/**
 * Custom autocomplete component.
 */
@Component({
  selector: 'es-autocomplete',
  templateUrl: './autocomplete.html',
})
export class AutocompleteComponent extends FormFieldComponent implements OnInit {

  /**
   * The data to perform autocomplete on.
   *
   * @memberof AutocompleteFieldComponent
   */
  @Input() controlArray: CompleterData;

  /**
   * The field label, defaults to empty, pass in a required label.
   */
  @Input() label?: string;

  /**
   * The field is optional, pass in any required static string.
   */
  @Input() leftText?: string;

  /**
   * The field is optional, pass in any required static string.
   */
  @Input() rightText?: string;

  /**
   * Minimum number of characters to initiate a search.
   *
   * @memberof AutocompleteFieldComponent
   */
  @Input() minLength?: number = 3;

  /**
   * We maintain our own state of touched, because there is a bug with the autocomplete input where it gets set as
   * touched as soon as clicked into.
   *
   * @memberof AutocompleteFieldComponent
   */
  public touched: boolean = false;

  /**
   * Constructor, does nothing.
   */
  constructor() {
    super();
  }

  ngOnInit() {
    if (this.parentFormGroup === undefined || this.submitted === undefined || this.controlName === undefined
      || this.controlArray === undefined) {
      throw new Error('Some required attributes were not passed in to the es-autocomplete component');
    }
  }

  /**
   * Gets the FormControl for the autocomplete field this component represents.
   */
  get autocompleteControl(): AbstractControl {
    return this.parentFormGroup.get(this.controlName);
  }
}

/**
 * Autocomplete module providing anything to do with autocomplete form elements.
 *
 * @export
 */
@NgModule({
  imports: [CommonModule, ReactiveFormsModule, Ng2CompleterModule],
  exports: [AutocompleteComponent, Ng2CompleterModule],
  declarations: [AutocompleteComponent],
})
export class AutocompleteModule {
}
