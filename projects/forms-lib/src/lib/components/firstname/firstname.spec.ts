import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstnameModule, FirstnameComponent } from './firstname';

describe('FirstnameComponent', () => {

  let fixture: ComponentFixture<FirstnameComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
      ],
      providers: [],
      imports: [
        FirstnameModule,
      ],
    });
    fixture = TestBed.createComponent(FirstnameComponent);
  });

  it('should throw an error if not all required attributes are passed in', () => {
    expect(() => fixture.detectChanges()).toThrowError();
  });

});
