import { Component, Input, OnInit, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AbstractControl, ReactiveFormsModule } from '@angular/forms';

import { FormFieldComponent } from '../form-field.component';

/**
 * Custom select component.
 */
@Component({
  selector: 'es-select',
  templateUrl: './select.html',
})
export class SelectComponent extends FormFieldComponent implements OnInit {

  /**
   * The field label, defaults to empty, pass in a required label.
   */
  @Input() label?: string;

  /**
   * The field is optional, pass in any required static string.
   */
  @Input() leftText?: string;

  /**
   * The field is optional, pass in any required static string.
   */
  @Input() rightText?: string;

  /**
   * The field is optional, pass in any array of key value pairs.
   */
  @Input() controlArray?: any[];

  /**
   * The field is optional, pass in any array of strings.
   */
  @Input() controlString?: string[];

  /**
   * The field is optional, defaults to true which will add in a Please select option to the top of the select list.
   */
  @Input() pleaseSelect?: boolean = true;

  /**
   * The value to use for Please Select if being shown as an option.
   */
  @Input() pleaseSelectLabel?: string = 'Please Select';

  /**
   * Constructor, does nothing.
   */
  constructor() {
    super();
  }

  ngOnInit() {
    if (this.parentFormGroup === undefined || this.submitted === undefined || this.controlName === undefined) {
      throw new Error('Some required attributes were not passed in to the es-select component');
    }
  }

  /**
   * Gets the FormControl for the text field this component represents.
   */
  get selectcontent(): AbstractControl {
    return this.parentFormGroup.get(this.controlName);
  }

}

/**
 * Select fields module providing anything to do with select form elements.
 *
 * @export
 */
@NgModule({
  imports: [CommonModule, ReactiveFormsModule],
  exports: [SelectComponent],
  declarations: [SelectComponent],
})
export class SelectModule {
}
