import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectComponent } from './select';
import { SelectModule } from './select';

describe('SelectComponent', () => {

  let fixture: ComponentFixture<SelectComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
      ],
      providers: [],
      imports: [
        SelectModule,
      ],
    });
    fixture = TestBed.createComponent(SelectComponent);
  });

  it('should throw an error if not all required attributes are passed in', () => {
    expect(() => fixture.detectChanges()).toThrowError();
  });

});
