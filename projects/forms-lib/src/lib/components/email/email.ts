import { Component, NgModule, OnInit, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, AbstractControl } from '@angular/forms';

import { FormFieldComponent } from '../form-field.component';

/**
 * Email component.
 */
@Component({
  selector: 'es-email',
  templateUrl: './email.html',
})
export class EmailComponent extends FormFieldComponent implements OnInit {

  /**
   * The field label, defaults to just Email address, so does not have to be passed in.
   */
  @Input() label: string = 'Email address';

  /**
   * Message to display if field has a required validation error, has a default so does not need to be passed in.
   */
  @Input() requiredMessage: string = 'Please enter a valid email address';

  /**
   * The value to use for the autocomplete attribute, has a default so does not have to be passed in.
   */
  @Input() autocomplete: string = 'username email';

  /**
   * Constructor, does nothing.
   */
  constructor() {
    super();
  }

  ngOnInit() {
    if (this.parentFormGroup === undefined || this.submitted === undefined || this.controlName === undefined) {
      throw new Error('Some required attributes were not passed in to the es-email component');
    }
  }

  /**
   * Gets the FormControl for the email field this component represents.
   */
  get email(): AbstractControl {
    return this.parentFormGroup.get(this.controlName);
  }

}

/**
 * Email module providing anything to do with email form fields.
 *
 * @export
 */
@NgModule({
  imports: [CommonModule, ReactiveFormsModule],
  exports: [EmailComponent],
  declarations: [EmailComponent],
})
export class EmailModule {
}
