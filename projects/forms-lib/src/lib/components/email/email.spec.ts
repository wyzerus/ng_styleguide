import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailComponent, EmailModule } from './email';

describe('EmailComponent', () => {

  let fixture: ComponentFixture<EmailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
      ],
      providers: [],
      imports: [
        EmailModule,
      ],
    });
    fixture = TestBed.createComponent(EmailComponent);
  });

  it('should throw an error if not all required attributes are passed in', () => {
    expect(() => fixture.detectChanges()).toThrowError();
  });

});
