import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostcodeModule, PostcodeComponent } from './postcode';

describe('PostcodeComponent', () => {

  let fixture: ComponentFixture<PostcodeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
      ],
      providers: [],
      imports: [
        PostcodeModule,
      ],
    });
    fixture = TestBed.createComponent(PostcodeComponent);
  });

  it('should throw an error if not all required attributes are passed in', () => {
    expect(() => fixture.detectChanges()).toThrowError();
  });

});
