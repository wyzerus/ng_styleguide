import { Component, Input, NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AbstractControl, ReactiveFormsModule } from '@angular/forms';

import { FormFieldComponent } from '../form-field.component';

/**
 * Custom postcode component.
 */
@Component({
  selector: 'es-postcode',
  templateUrl: './postcode.html',
})
export class PostcodeComponent extends FormFieldComponent implements OnInit {

  /**
   * The field label, defaults to just Postcode, so does not have to be passed in.
   */
  @Input() label?: string = 'Postcode';

  /**
   * Additional text to display underneath the label.
   */
  @Input() helpText?: string;

  /**
   * Constructor, does nothing.
   */
  constructor() {
    super();
  }

  ngOnInit() {
    if (this.parentFormGroup === undefined || this.submitted === undefined || this.controlName === undefined) {
      throw new Error('Some required attributes were not passed in to the es-postcode component');
    }
  }

  /**
   * Gets the FormControl for the postcode field this component represents.
   */
  get postcodeControl(): AbstractControl {
    return this.parentFormGroup.get(this.controlName);
  }

}
/**
 * Postcode module providing anything to do with postcode form fields.
 *
 * @export
 */
@NgModule({
  imports: [CommonModule, ReactiveFormsModule],
  exports: [PostcodeComponent],
  declarations: [PostcodeComponent],
})
export class PostcodeModule {
}
